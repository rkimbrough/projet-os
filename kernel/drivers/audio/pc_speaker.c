#include "pc_speaker.h"
#include "timer/pit.h"

#include <kernel/portio.h>
#include <kernel/timer.h>
#include <kernel/tty.h>

#include <stdbool.h>

static bool playing;
audio_channel pcspk_uniquech = { "PCSPK" };

//-- Fonctions pour communiquer avec le port du haut-parleur --
void pcspk_set_flag(pcspk_flag f)
{
	outb(SPK_PORT, inb(SPK_PORT) | f);
}

void pcspk_clear_flag(pcspk_flag f)
{
	outb(SPK_PORT, inb(SPK_PORT) & ~f);
}

uint8_t pcspk_check_flag(pcspk_flag f)
{
	return inb(SPK_PORT) & f;
}

//-- Fonction pour gérer le son, en interne --
static inline void play_sound()
{
	pcspk_set_flag(PCSPK_GATE);
	playing = true;
}

static inline void stop_sound()
{
	pcspk_clear_flag(PCSPK_GATE);
	playing = false;
}

//-- Fonctions du driver générique --
void pcspk_driver_init()
{
	stop_sound();
	pcspk_set_flag(PCSPK_CONNECT_PIT);
	term_writestring("PC Speaker initialized.\n");
}

void pcspk_driver_update() {}

const char *pcspk_get_device_name(void *device)
{
	return ((audio_channel *)device)->name;
}

//-- Fonctions du driver spécifique --
audio_channel *pcspk_add_channel(const char *)
{
	return &pcspk_uniquech;
}

void pcspk_play_sound(audio_channel *, uint32_t freq)
{
	pit_program_ch(PIT_SPK, PIT_ACC_WHOLE, PIT_SQUARE, PIT_BASE_FREQ / freq);
	if (!playing)
		play_sound();
}

void pcspk_stop_sound(audio_channel *)
{
	if (playing)
		stop_sound();
}

//-- Driver --
audio_driver pcspk_driver = {
	{
		"PCSPK",
		&pcspk_driver_init,
		&pcspk_driver_update,
		&pcspk_get_device_name
	},
	&pcspk_add_channel,
	&pcspk_play_sound,
	&pcspk_stop_sound
};

