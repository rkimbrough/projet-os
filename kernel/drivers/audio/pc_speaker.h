#ifndef SPEAKER_H
#define SPEAKER_H 1

#include <kernel/audio.h>

#ifdef __arch_i386__
#define SPK_PORT 0x61
#endif

typedef enum _pcspk_flag
{
	PCSPK_CONNECT_PIT	= 1 << 0,
	PCSPK_GATE			= 1 << 1,
	PCSPK_OUT			= 1 << 5
} pcspk_flag;

struct _audio_channel
{
	const char *name;
};

#endif //SPEAKER_H
