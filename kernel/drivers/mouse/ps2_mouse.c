#include "ps2_mouse.h"

#include <kernel/interrupt.h>
#include <kernel/portio.h>
#include <kernel/tty.h>
#include <stdio.h>

void mouse_wait(){
	uint8_t i;
    do {
		i = inb(0x64); // check if there is data
	} while ( ((i & 0x01) == 0) || ((i & 0x20) == 0)); // 0x20 -> check if mouse and not keyboard
}

uint8_t mouse_read(){
    //mouse_wait();
    return inb(0x60);
}


__attribute__((interrupt))
void isr_mouse(interrupt_frame *frame __attribute__((unused))){
	printf("mouse isr\n"); // never called WTF ?

	// byte 1 ->  | Y overflow | X overflow | Y sign bit | X sign bit | 1 | Middle button | Right button | Left button |
	uint8_t curByte = mouse_read(); // get the first byte
	if(curByte == 0xFA || (curByte & 0x80) || (curByte & 0x40)){ // acknowledge or overflow
		send_EOI(MOUSE_INT);
		return;
	}
	char yNegative = (curByte & 0x20); // check if y negative
	char xNegative = (curByte & 0x10); // check if x negative

	char isMiddlePressed = (curByte & 0x04); // check if the middle button has been pressed
	char isRightPressed  = (curByte & 0x02); // check if the right button has been pressed
	char isLeftPressed   = (curByte & 0x01); // check if the left button has been pressed

	// byte 2 ->  X movement
	uint8_t xPos = mouse_read();

	// byte 3 ->  Y movement
	uint8_t yPos = mouse_read();

	printf("Is middle pressed? %d\n", isMiddlePressed);
	printf("Is right pressed? %d\n", isRightPressed);
	printf("Is left pressed? %d\n", isLeftPressed);

	if(yNegative) yPos |= 0xFFFFFF00;
	if(xNegative) xPos |= 0xFFFFFF00;

	printf("Mouse pos: (%d, %d)\n", xPos, yPos);

	send_EOI(MOUSE_INT);
}

void ps2_mouse_init(){
	set_interrupt(IRQ_START + MOUSE_INT, isr_mouse);
	enable_irq(MOUSE_INT);
	outb(0x64, 0xD4);
	while (inb(0x60) & 2) {}
	outb(0x60, 0xF4);
	term_writestring("Mouse initialized.\n");//while (1) {}
}
