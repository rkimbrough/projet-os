#ifndef __MOUSE_H__
#define __MOUSE_H__ 1

#include <kernel/tty.h>
#include <kernel/portio.h>

// Wait for a mouse interrupt
void mouse_wait();

// Read mouse data
uint8_t mouse_read();

// Init the mouse
void ps2_mouse_init();

#endif // __MOUSE_H__
