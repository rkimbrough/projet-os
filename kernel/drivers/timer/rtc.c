//http://www.walshcomptech.com/ohlandl/config/cmos_bank_0.html#Hex_000

#include "cmos/cmos.h"

#include <kernel/timer.h>
#include <kernel/interrupt.h>
#include <kernel/tty.h>

#include <stdbool.h>

static bool form24;
static bool formbin;
volatile uint64_t real_ticks;
volatile int date[6];

//-- Fonctions pour lire les registres de temps au bon format --
static inline int read_time_reg(cmos_register reg)
{
	int val = cmos_read_reg(reg);
	if (!formbin)
		val = (val >> 4) * 10 + (val & 0x0F);
	return val;
}

static inline int read_hour_reg()
{
	int val = cmos_read_reg(CMOS_HOURS);
	int offset = 0;
	//En mode 12 heures, le bit de poids fort est utilisé pour indiquer l'après-midi
	if (!form24)
	{
		offset = 12 * (val >> 7);
		val &= ~(1 << 7);
	}

	if (!formbin)
		val = (val >> 4) * 10 + (val & 0x0F);
	//En mode 12 heures, midi (minuit) est 12am (12pm)
	if (!form24)
		val = val % 12 + offset;
	return val;
}

//-- ISR --
__attribute__((interrupt))
void rtc_isr(interrupt_frame *frame __attribute__((unused)))
{
	uint8_t regC = cmos_read_reg(CMOS_INT_INFO);
	if (regC & CMOS_B_TICK_INT)
		real_ticks++;
	if (regC & CMOS_B_UPD_INT)
	{
		date[DATE_SECONDS] = read_time_reg(CMOS_SECONDS);
		date[DATE_MINUTES] = read_time_reg(CMOS_MINUTES);
		date[DATE_HOURS] = read_hour_reg();
		date[DATE_DAY] = read_time_reg(CMOS_DAY);
		date[DATE_MONTH] = read_time_reg(CMOS_MONTH);
		date[DATE_YEAR] = read_time_reg(CMOS_YEAR);
	}

	send_EOI(RTC_INT);
}

void printbin(uint8_t n)
{
	term_writestring("0b");
	for (int i = 7; i >= 0; i--)
		term_putchar('0' + (1 & n >> i));
	term_putchar('\n');
}

//-- Fonctions du driver --
void rtc_driver_init()
{
	real_ticks = 0;
	disable_nmi();
	//On choisit la fréquence des ticks
	uint8_t regA = cmos_read_reg(CMOS_STATUS_A);
	cmos_write_reg(CMOS_STATUS_A, (regA & 0xF0) | 3);

	set_interrupt(IRQ_START + RTC_INT, rtc_isr);
	enable_irq(RTC_INT);
	//On active les ticks et le temps réel
	uint8_t regB = cmos_read_reg(CMOS_STATUS_B);
	form24 = regB & CMOS_B_24H;
	formbin = regB & CMOS_B_BIN;
	cmos_write_reg(CMOS_STATUS_B, regB | CMOS_B_TICK_INT | CMOS_B_UPD_INT);
	enable_nmi();
	term_writestring("RTC initialized.\n");
}

void rtc_driver_update() {}

const char *rtc_get_device_name(void *)
{
	return "RTC";
}

//-- Driver --
timer_driver rtc_driver = {
	{
		"RTC",
		&rtc_driver_init,
		&rtc_driver_update,
		&rtc_get_device_name
	}
};
