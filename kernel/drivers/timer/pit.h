/*
Le PIT contient un oscillateur avec une fréquence de base de 1193.182Hz. Il y a
trois horloges différentes, qui servent plusieurs fonctions :
	- 0x40 : horloge reliée à IRQ0
	- 0x41 : obsolète
	- 0x42 : générateur de fréquences pour le haut-parleur

En outre, un registre de commande à 0x43 peut être écrit pour programmer chaque
horloge.
*/

#ifndef PIT_H
#define PIT_H 1

#include <stdint.h>

#ifdef __arch_i386__
#define PIT_OUT 0x40
#define PIT_CMD_REG 0x43
#endif

#define PIT_BASE_FREQ 1193182
#define PIT_MIN_FREQ 18

typedef enum _pit_mode
{
	PIT_ONESHOT,
	PIT_ONESHOT_HARDWARE,
	PIT_REPEAT,
	PIT_SQUARE,
	PIT_STROBE,
	PIT_STROBE_HARDWARE
} pit_mode;

typedef enum _pit_access
{
	PIT_ACC_LOW = 1,
	PIT_ACC_HIGH,
	PIT_ACC_WHOLE
} pit_access;

typedef enum _pit_channel
{
	PIT_IRQ,
	PIT_UNUSED,
	PIT_SPK
} pit_channel;

void pit_program_ch(pit_channel ch, pit_access acc, pit_mode mod, uint16_t count);

#endif
