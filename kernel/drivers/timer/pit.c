#include "pit.h"

#include <kernel/timer.h>
#include <kernel/interrupt.h>
#include <kernel/portio.h>
#include <kernel/tty.h>

/*
Pas besoin d'une file de commandes, on ne reçoit généralement pas de réponse
de la puce.
*/

//-- ISR --
volatile uint64_t sys_ticks;
__attribute__((interrupt))
void pit_isr(interrupt_frame *frame __attribute__((unused)))
{
	sys_ticks++;
	send_EOI(PIT_INT);
}

//-- Programmer un seul canal --
void pit_program_ch(pit_channel ch, pit_access acc, pit_mode mod, uint16_t count)
{
	outb(PIT_CMD_REG, ch << 6 | acc << 4 | mod << 1);
	if (acc & 0b1)
		outb(PIT_OUT + ch, count & 0xFF);
	if (acc & 0b10)
		outb(PIT_OUT + ch, count >> 8);
}

//-- Fonctions du driver --
void pit_driver_init()
{
	sys_ticks = 0;
	pit_program_ch(PIT_IRQ, PIT_ACC_WHOLE, PIT_REPEAT, PIT_BASE_FREQ / SYS_TICK_RATE);

	set_interrupt(IRQ_START + PIT_INT, &pit_isr);
	enable_irq(PIT_INT);
	term_writestring("PIT initialized.\n");
}

void pit_driver_update() {}

const char *pit_get_device_name(void *)
{
	return "PIT";
}

//-- Driver --
timer_driver pit_driver = {
	{
		"PIT",
		&pit_driver_init,
		&pit_driver_update,
		&pit_get_device_name
	}
};
