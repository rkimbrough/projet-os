#include <kernel/commands.h>

void reset_cmd(cmdq *queue)
{
	queue->front = 0;
	queue->back = 0;
	queue->size = 0;
}

void queue_cmd(cmdq *queue, cmd_data_t data, cmd_sender_t routine)
{
	if (queue->size == QUEUE_SIZE)
		{}//panic
	queue->buff[queue->back].data = data;
	queue->buff[queue->back].routine = routine;
	queue->back = (queue->back + 1) % QUEUE_SIZE;
	queue->size++;
}

cmd_t front_cmd(cmdq *queue)
{
	if (queue->size == 0)
		{}//panic
	return queue->buff[queue->front];
}

void pop_cmd(cmdq *queue)
{
	if (queue->size == 0)
		{}//panic
	queue->front = (queue->front + 1) % QUEUE_SIZE;
	queue->size--;
}

bool has_cmd(cmdq *queue)
{
	return queue->size > 0;
}
