#include "ps2_controller.h"

#include <kernel/portio.h>

static bool _keyboard_status = false;

void ps2_poll_output()
{
	while (!(inb(PS2_CONTROLLER_PORT) & 1));
}

void ps2_poll_input()
{
	while (inb(PS2_CONTROLLER_PORT) & 2);
}

// récupérer le statut du controleur
uint8_t kybrd_ctrl_read_status () {
	return(inb(PS2_CONTROLLER_PORT));
}

// récupérer le statut de l'encodeur
uint8_t kybrd_encd_read_status () {
	return(inb(PS2_ENCODER_PORT));
}

// envoyer une commande au controleur
void kybrd_ctrl_send_cmd(uint8_t cmd) {
	uint8_t status;

	do {
		// on vérifie que le controleur peut prendre une commande
		status = kybrd_ctrl_read_status();
	} while (status & KYBRD_CTRL_STATS_MASK_IN_BUF);
	// on envoie au controleur
	outb(PS2_CONTROLLER_PORT, cmd);
}

// envoyer une commande à l'encodeur
void kybrd_encd_send_cmd(uint8_t cmd) {
	uint8_t status;

	do {
		// on vérifie que le controleur peut prendre une commande
		status = kybrd_ctrl_read_status();
	} while (status & KYBRD_CTRL_STATS_MASK_IN_BUF);
	// on envoie à l'encodeur ATTENTION via le ctrl donc on doit faire la boucle d'au dessus
	outb(PS2_ENCODER_PORT, cmd);
}

// -- COMMANDES VERS ENCODEUR --

// set leds
void kybrd_set_leds(bool scroll, bool num, bool caps) {
	uint8_t data = 0;
	data = (scroll) ? (data | 1) : (data & 1);
	data = (num) ? (data | 2) : (data & 2);
	data = (caps) ? (data | 3) : (data & 3);

	kybrd_encd_send_cmd(KYBRD_ENC_CMD_SET_LED);
	kybrd_encd_send_cmd(data);
}

// 0 : renvoie le sc actuel sur 0x60, 1,2,3 : set le sc correspondant
void kybrd_set_scan_code(int scan_code_command) {
	uint8_t data = 0;
	switch (scan_code_command) {
		case 0:
			data = data | 1;
			break;
		case 1:
			data = data | 2;
			break;
		case 2:
			data = data | 4;
			break;
		case 3:
			data = data | 8;
			break;
	}

	kybrd_encd_send_cmd(KYBRD_ENC_CMD_SET_SCC);
	kybrd_encd_send_cmd(data);
}
// rpt_rate : 0 : 30 char/sec -> 31 : 2 chars/sec
// rpt_delay : 1/4 -> 1 sec par bit
void kybrd_set_repeat(uint8_t rpt_rate, uint8_t rpt_delay) {
	uint8_t data = 0;
	data = (data | rpt_rate) | (rpt_delay << 5);

	kybrd_encd_send_cmd(KYBRD_ENC_CMD_SET_RPT);
	kybrd_encd_send_cmd(data);
}

// send command to encoder
void kybrd_encd_generic_cmd(uint8_t cmd_code) {
	kybrd_encd_send_cmd(cmd_code);
}

// send command to a specific key encoder
void kybrd_spec_autorls(uint8_t cmd_code, uint8_t scan_code) {
	kybrd_encd_send_cmd(cmd_code);
	kybrd_encd_send_cmd(scan_code);
}

// -- COMMANDES VERS CONTROLEUR --

bool kybrd_self_test_ctrl() {
	kybrd_ctrl_send_cmd(KYBRD_CTRL_CMD_SLF_TST);

	while(1)
		if (kybrd_ctrl_read_status() & KYBRD_CTRL_STATS_MASK_OUT_BUF)
			break;

	// 0x55 self test passed 
	return (kybrd_encd_read_status() == 0x55) ? true : false;
}

void kybrd_disable_ctrl() {
	kybrd_ctrl_send_cmd(KYBRD_CTRL_CMD_DIS);
	_keyboard_status = false;
}


void kybrd_enable_ctrl() {
	kybrd_ctrl_send_cmd(KYBRD_CTRL_CMD_ENB);
	_keyboard_status = true;
}

//-- Les trucs de Yannis --
void poll_in()
{
	while (inb(0x64) & 2) {}
}

/**
 * Tells if it is a dual channel PS2 controller
*/
static uint8_t ps2_is_dual_channel = 1;

/**
 * Tells if the ps2 controller exists
 * @return 1 if it does 0 otherwise
*/
uint8_t ps2_ctrl_exists(){
    // we don't have an APCI so always true
    return 1;
}

/**
 * Disable ps2 devices
*/
void ps2_dsbl_dev(){
	poll_in();
    outb(0x64, PS2_CTRL_CMD_DSBL_FST);
	poll_in();
    outb(0x64, PS2_CTRL_CMD_DSBL_SND);
}

/**
 * Flush the ouput buffer
*/
void ps2_flush_out_buf(){
    // just read form io port 0x60
	while (inb(0x64) & 1)
		inb(0x60);
}

/**
 * Read the controller config byte
 * @return The config byte
*/
uint8_t ps2_read_ctrl_cfg_byte(){
	poll_in();
    outb(0x64, PS2_CTRL_CMD_READ_BYTE_0);
	ps2_wait();
	return inb(0x60);
}

/**
 * Clear a config bit
 * @param config The controller config byte
 * @param mask The bit to clear
*/
void ps2_ctrl_conf_clear_bit(uint8_t* config, ps2_ctrl_cfg_byte mask){
    *config &= ~mask;
}

/**
 * Set a config bit
 * @param config The controller config byte
 * @param mask The bit to set
*/
void ps2_ctrl_conf_set_bit(uint8_t* config, ps2_ctrl_cfg_byte mask){
    *config |= mask;
}

/**
 * Check a config bit
 * @param config The controller config byte
 * @param mask The bit to check
 * @return 1 if the bit was set, 0 otherwise
*/
uint8_t ps2_ctrl_conf_check_bit(uint8_t* config, ps2_ctrl_cfg_byte mask){
    return *config & mask;
}

/**
 * Wait for a signal
*/
void ps2_wait(){
    uint8_t i;
	do {
		i = inb(0x64);
	} while ((i & 1) == 0);
}

/**
 * Set the controller configuration byte
 * @param newConf The new configuration byte
*/
void ps2_set_ctrl_conf_byte(uint8_t newConf){
	poll_in();
	outb(0x64, PS2_CTR6_CMD_WRITE_BYTE_0);
	poll_in();
    outb(0x60, newConf);
}

/**
 * Init the controller configuration byte and test if it can be a dual channel
*/
void ps2_init_ctrl_conf_byte(){
    // read config byte
    uint8_t confb = ps2_read_ctrl_cfg_byte();
    
    // check if it is a dual channel
    if(!ps2_ctrl_conf_check_bit(&confb, PS2_CTRL_CFG_BYTE_SND_CLK)){
        // controller can't be a dual channel
        ps2_is_dual_channel = 0;
    }

    // clear bits
    ps2_ctrl_conf_clear_bit(&confb, PS2_CTRL_CFG_BYTE_FST_INTERRUPT);
    ps2_ctrl_conf_clear_bit(&confb, PS2_CTRL_CFG_BYTE_SND_INTERRUPT);
    ps2_ctrl_conf_clear_bit(&confb, PS2_CTRL_CFG_BYTE_FST_TRANSLATION);

    // write new config byte
    ps2_set_ctrl_conf_byte(confb);
}

/**
 * Controller self test
 * @return 1 if it worked, 0 otherwise
*/
uint8_t ps2_ctrl_self_test(){
    // read config byte to restore it later
    uint8_t confb = ps2_read_ctrl_cfg_byte();
    // send test command
	poll_in();
	outb(0x64, PS2_CTRL_CMD_TEST_CTRL);
    // wait for the answer
    ps2_wait();
    // get answer
    uint8_t answer = inb(0x60);
    // check if it worked
    switch(answer){
        case PS2_CTRL_CMD_TEST_CTRL_FAILED:{ // test failed
            term_writestring("PS2 routine, step 6: controller self test failed!\n");
            return 0;
        }
        case PS2_CTRL_CMD_TEST_CTRL_PASSED:{ // test is a success
            term_writestring("PS2 routine, step 6: controller self test passed!\n");
            break;
        }
        default:{ // unkown value
            term_writestring("PS2 routine, step 6: unkown answer, abort the mission !\n");
            return 0;
        }
    }

    // restore config byte
    ps2_set_ctrl_conf_byte(confb);
    return 1;
}

/**
 * Test if there are 2 channels
*/
void ps2_check_2_channels(){
    // if single channel from step 5
    if(!ps2_is_dual_channel)
        return;
    // enable second port
	poll_in();
    outb(0x64, PS2_CTRL_CMD_ENBL_SND);
    // read config byte
    uint8_t confb = ps2_read_ctrl_cfg_byte();
    // check if it is a dual channel
    if(ps2_ctrl_conf_check_bit(&confb, PS2_CTRL_CFG_BYTE_SND_CLK)){
        // controller can't be a dual channel
        ps2_is_dual_channel = 0;
    }
    // disable second port
	poll_in();
    outb(0x64, PS2_CTRL_CMD_DSBL_SND);
}

/**
 * Test the ports
 * @return 0 if something went wrong, 1 otherwise
*/
uint8_t ps2_check_ports(){
    uint8_t response = 0;

    // test first port
	poll_in();
    outb(0x64, PS2_CTRL_CMD_TEST_FST);
    ps2_wait();
    response = inb(0x60);
    if (response != PS2_CTRL_CMD_TEST_FST_PASSED)
    {
		term_writestring("aaa :(\n");
		return 0;
	}

    // test second port
    if (!ps2_is_dual_channel)
	{
        return 1;
	}

	poll_in();
    outb(0x64, PS2_CTRL_CMD_TEST_SND);
    ps2_wait();
    response = inb(0x60);
    if(response != PS2_CTRL_CMD_TEST_SND_PASSED)
    {
		term_writestring("bbb :(\n");
		return 0;
	}

    return 1;
}

/**
 * Activate the ports and their interrupts
*/
void ps2_enable_ports(){
    // enable first port
	poll_in();
    outb(0x64, PS2_CTRL_CMD_ENBL_FST);
    // read config byte
    uint8_t confb = ps2_read_ctrl_cfg_byte();
    // update config byte
    ps2_ctrl_conf_set_bit(&confb, PS2_CTRL_CFG_BYTE_FST_INTERRUPT);
    ps2_ctrl_conf_set_bit(&confb, PS2_CTRL_CFG_BYTE_FST_TRANSLATION);
    
    // dual channel
    if(ps2_is_dual_channel){
		term_writestring("ouiii\n");
        // enable second port
		poll_in();
        outb(0x64, PS2_CTRL_CMD_ENBL_SND);
        // set config byte
        ps2_ctrl_conf_set_bit(&confb, PS2_CTRL_CFG_BYTE_SND_INTERRUPT);
    }
    // set config byte
    ps2_set_ctrl_conf_byte(confb);
}

/**
 * Initiate the ps2 controller routine
 * @return 0 if something went wrong, 1 otherwise
*/
uint8_t ps2_routine(){
	term_writestring("Begin PS2 routine.\n");//while (1) {}
    // step1 
    // TODO:
    // step2
    if(!ps2_ctrl_exists()) return 0;
    // step 3
    ps2_dsbl_dev();
    // step 4
    ps2_flush_out_buf();
    // step 5
    ps2_init_ctrl_conf_byte();
	// step 6
    if(!ps2_ctrl_self_test()) return 0;
    // step 7
    ps2_check_2_channels();
    // step 8
	if(!ps2_check_ports()) return 0;
    // step 9
    ps2_enable_ports();
    // step 10
    // TODO:
	term_writestring("End PS2 routine.\n");//while (1) {}
    return 1;
}
