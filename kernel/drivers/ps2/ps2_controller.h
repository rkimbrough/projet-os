/*

On envoie des commandes (de 0xED à 0xFF (avec 0xF7 -> 0xFD inclu pour scan code 3 
seulement) au clavier et il répond (ACK 0xFA ou Resend 0xFE ou 0xEE Echo et des tests 
variés pour 0xFF).

On doit envoyer une commande à la fois et être prêt à Resend.

File de commande : ACK on pop cmd et on passe à la suivante, Resend on recommence.

Etat de machine : changement d'état quand des commandes s'exécutent avec succès.

3 scan code le défault c'est 2 celui que je vais essayer d'implémenter ici.

Ce sont des séquences de 1 ou plusieurs octets. Faudrait faire une table pour recensser 
les séquences possibles d'octets.

Pour les code de touche (key codes), y a rien de prédéterminé mais on devrait s'inspirer 
de trucs déjà existants et extensibles à d'autres périphériques comme la souris.

Quand l'état machine sait qu'il a reçu un scan code complet, on transforme ce scan 
code en key code.

Beaucoup d'informations sur la gestion des données dans la page "8042"_PS/2_Controller 
du wiki.
Les ports IO sont : 
0x60 | Read/Write | Data
0x64 | Read       | Status
0x64 | Write      | Command

0x60 : données reçues depuis clavier et écrire données vers le clavier

---
Broken Thorn : 

Keyb encoder, dans le kb qui envoie les scan codes
Keyb controller, dans la carte mère qui fait l'interface I/O
ON DOIT COMMUNIQUER AVEC LES DEUX !!!

2 types de scan code uniques pour chaque touche : Make (touche appuyé ou maintenue) 
Break (touche relachée) -> ensemble constitue le Scan Code Set

Les scan code sont sauvegardés dans le keyb controller

Types d'interface : 
- Polling : checker si une touche est appuyée ou pas avec le kb controller
- Interrupt Request : on peut faire en sorte que le kb controller nous envoie une interrupt 
a chaque fois qu'une touche est appuyé ou pas

0x60 sert pour accéder à l'encoder alors que 0x64 au controller.
Write envoie une commande
Read lit le buffer (0x60) ou le registre de statut (0x64)

Registre de statut (0x64) 8 bits : 

0: Output buffer status : 0 -> empty ; 1 -> full (prêt à lire)
1: Input buffer status : 0 -> empty (prêt pour écrire) ; 1 -> full 
2: System flag (1 si test ok)
3: Command data : 0 -> dernier write sur 0x60 ; 1 -> dernier write sur 0x64
4: Keyboard Locked (1 si vérouillé)
5: Auxiliary buffer full 
6: Timeout 
7: Parity error

Quand on envoie une commande à 0x60 on doit s'assurer que le controller est prêt 
à recevoir étant donné que la commande va aussi passé par le controller.

Commandes pour le controller remarquables : 
- 0x20 : Lecture du byte de commande (et la RAM pcq c'est la même chose, le byte de 
commande est dans la RAM) (pas utile)
- 0x60 : Ecriture byte de commande (pas utile dans la démo)
- cf l'enum en question

*/

#ifndef PS2_CONTROLLER_H
#define PS2_CONTROLLER_H 1

#include <kernel/tty.h>

#include <stdint.h>
#include <stdbool.h>

#ifdef __arch_i386__
#define PS2_ENCODER_PORT	0x60
#define PS2_CONTROLLER_PORT	0x64
#endif

typedef enum _ps2_status_register {
	KYBRD_CTRL_STATS_MASK_OUT_BUF	=	1,		  //00000001
	KYBRD_CTRL_STATS_MASK_IN_BUF	=	2,		  //00000010
	KYBRD_CTRL_STATS_MASK_SYSTEM	=	4,		  //00000100
	KYBRD_CTRL_STATS_MASK_CMD_DATA	=	8,		//00001000
	KYBRD_CTRL_STATS_MASK_LOCKED	=	0x10,		//00010000
	KYBRD_CTRL_STATS_MASK_AUX_BUF	=	0x20,		//00100000
	KYBRD_CTRL_STATS_MASK_TIMEOUT	=	0x40,		//01000000
	KYBRD_CTRL_STATS_MASK_PARITY	=	0x80		//10000000
} ps2_status_register;

// commandes pour l'encodeur
enum KYBRD_ENC_CMD {
	KYBRD_ENC_CMD_SET_LED         = 0xED, // set leds
	KYBRD_ENC_CMD_ECHO            = 0xEE, // echo command
	KYBRD_ENC_CMD_SET_SCC         = 0xF0, // set alternate scan code
	KYBRD_ENC_CMD_ID              = 0xF2, // demande id (lue sur 0x60)
	KYBRD_ENC_CMD_SET_RPT         = 0xF3, // set autorepeat delay & repeat rate
	KYBRD_ENC_CMD_ENB             = 0xF4, // enable kbd
	KYBRD_ENC_CMD_DIS             = 0xF5, // disable kbd + paramètres par défaut (parfois)
	KYBRD_ENC_CMD_DFT             = 0xF6, // met les paramètres par défaut du clavier
	KYBRD_ENC_CMD_AUTORPT         = 0xF7, // SC 3 seulement : tout le monde en autorepeat
	KYBRD_ENC_CMD_MKE_RLS         = 0xF8, // SC 3 seulement : tout le monde en make/release
	KYBRD_ENC_CMD_MKE             = 0xF9, // SC 3 seulement : tout le monde en make
	KYBRD_ENC_CMD_MKE_RLS_AUTORPT = 0xFA, // SC 3 seulement : tout le monde en te
	KYBRD_ENC_CMD_SPC_AUTORPT     = 0xFB, // SC 3 seulement : 1 en autorpt 
	KYBRD_ENC_CMD_SPC_MKE_RLS     = 0xFC, // SC 3 seulement : 1 en make/release 
	KYBRD_ENC_CMD_SPC_MKE             = 0xFD, // SC 3 seulement : 1 en make
	KYBRD_ENC_CMD_RSD             = 0xFE, // renvoie du dernier résultat
	KYBRD_ENC_CMD_RST_TEST        = 0xFF, // réinitialise et test
};

enum KYBRD_ENC_RET_CODE {
	KYBRD_ENC_RET_BUFF_OVF = 0x0,  // buffer overflow
	KYBRD_ENC_RET_BAT_OK   = 0xAA, // self test passed
	KYBRD_ENC_RET_ACK      = 0xFA, // acknowledged
	KYBRD_ENC_RET_BAT_FL_1 = 0xFC, // self test failed
	KYBRD_ENC_RET_BAT_FL_2 = 0xFD, // self test failed
	KYBRD_ENC_RET_RSD      = 0xFE, // resend
	KYBRD_ENC_RET_KEY_ERR  = 0xFF, // key error
};

enum KYBRD_CTRL_CMD {
	KYBRD_CTRL_CMD_SLF_TST  = 0xAA, // self test 0x55 sur 0x60 : success !
	KYBRD_CTRL_CMD_INTF_TST = 0xAB, // interface test
	KYBRD_CTRL_CMD_DIS      = 0xAD, // keyboard disable
	KYBRD_CTRL_CMD_ENB      = 0xAE, // keyboard enable
};

//-- Fonctions --
// lecture
void ps2_poll_output();
void ps2_poll_input();

// envoi de commandes
void kybrd_ctrl_send_cmd(uint8_t cmd);
void kybrd_enc_send_cmd(uint8_t cmd);

// commandes vers encodeur
void kybrd_set_leds(bool scroll, bool num, bool caps);
void kybrd_set_scan_code(int scan_code_command);
void kybrd_set_repeat(uint8_t rpt_rate, uint8_t rpt_delay);

void kybrd_encd_generic_cmd(uint8_t cmd_code);
void kybrd_encd_spec_key_cmd(uint8_t cmd_code, uint8_t scan_code);

// commandes vers controller
bool kybrd_self_test_ctrl();
void kybrd_disable_ctrl();
void kybrd_enable_ctrl();

//-- Les trucs de Yannis, on fusionne les fichiers parce que c'est pas très utile d'en avoir plus que ça --
/**
https://wiki.osdev.org/%228042%22_PS/2_Controller


############################## CONFIG ##################################

PS2, ports IO : 
0x60 | Read/Write | Data
0x64 | Read       | Status
0x64 | Write      | Command

0x60 : données reçues depuis clavier et écrire données vers le clavier


Le status register contient des flags représentant l'état du controlleur PS2:
0 | Output buffer status (0 = empty, 1 = full) | Must be set before reading data from IO port 0x60
1 | Input buffer status (0 = empty, 1 = full)  | Must be set before writing data to IO port 0x60 or 0x64
2 | System flag                                | Meant to be cleared on reset and set by firmware (via. PS/2 Controller Configuration Byte) if the system passes self tests (POST) 
3 | Command / Data                             | (0 = data written to input buffer is data for PS/2 device, 1 = data written to input buffer is data for PS/2 controller command)
4 | Unknown (chipset specific)                 | May be "keyboard lock" (more likely unused on modern systems)
5 | Unknown (chipset specific)                 | May be "receive time-out" or "second PS/2 port output buffer full"
6 | Time-out error (0 = no error, 1 = time-out error)
7 | Parity error (0 = no error, 1 = time-out error)

Le byte de configuration du controlleur est de la forme:
0 | First PS/2 port interrupt (1 = enabled, 0 = disabled)
1 | Second PS/2 port interrupt (1 = enabled, 0 = disabled, only if 2 PS/2 ports supported)
2 | System Flag (1 = system passed POST, 0 = your OS shouldn't be running)
3 | Should be zero (according to OSdev but QEMU says it is setable)
4 | First PS/2 port clock (1 = disabled, 0 = enabled)
5 | Second PS/2 port clock (1 = disabled, 0 = enabled, only if 2 PS/2 ports supported)
6 | First PS/2 port translation (1 = enabled, 0 = disabled)
7 | Must be zero


############################## INTITIALISATION #######################################

Pour initialiser les controlleurs PS2 il faut suivre une routine spéciale.

Étape 1: 
initialiser les controlleurs usb et desactiver le `USB legacy support` qui peut interférer avec le controlleur PS2
(long et fastidieux du coup TODO)

Étape 2:
déterminer si le controlleur PS2 existe, comme on n'a pas de ACPI, on sait qu'il y a un PS/2 controller

Étape 3:
On désactive tous les apareils PS2 pour s'assurer qu'ils n'envoient pas de données au mauvais moment

Étape 4:
On vide le buffer du controlleur pour être sûr qu'aucune données ne le bloque.

Étape 5:
On met à jour le byte de configuration du controlleur

Étape 6:
On fait des tests

Étape 7:
On regarde s'il peut y avoir 2 channels

Étape 8:
On test les ports

Étape 9:
On active les 2 ports et on active leurs interruptions

*/

#include <stdint.h>

typedef enum _ps2_ctrl_cfg_byte{
    PS2_CTRL_CFG_BYTE_FST_INTERRUPT   = 0x01, // first port interrupt 
    PS2_CTRL_CFG_BYTE_SND_INTERRUPT   = 0x02, // second port interrupt 
    PS2_CTRL_CFG_BYTE_SYS             = 0x04, // system flag
    PS2_CTRL_CFG_BYTE_UKNW            = 0x08, // should be 0 but not sure
    PS2_CTRL_CFG_BYTE_FST_CLK         = 0x10, // first port clock
    PS2_CTRL_CFG_BYTE_SND_CLK         = 0x20, // second port clock
    PS2_CTRL_CFG_BYTE_FST_TRANSLATION = 0x40, // first port translation
    PS2_CTRL_CFG_BYTE_ZERO            = 0x80, // must be zero
}ps2_ctrl_cfg_byte;

typedef enum _ps2_status_reg{
    PS2_STATUS_REG_OUT      = 0x01, // output buffer status
    PS2_STATUS_REG_IN       = 0x02, // input buffer status
    PS2_STATUS_REG_SYS      = 0x04, // system flag
    PS2_STATUS_REG_CMD_DATA = 0x08, // command / data
    PS2_STATUS_REG_UKNW_1   = 0x10, // chipset specific
    PS2_STATUS_REG_UKNW_2   = 0x20, // chipset specific
    PS2_STATUS_REG_TIMEOUT  = 0x40, // timeout error
    PS2_STATUS_REG_PARITY   = 0x80, // parity error
}ps2_status_reg;

typedef enum _ps2_ctrl_cmd_read{
    PS2_CTRL_CMD_READ_BYTE_0  = 0x20, // read byte 00 from internal ram
    PS2_CTRL_CMD_READ_BYTE_01 = 0x21, // read byte 01 from internal ram
    PS2_CTRL_CMD_READ_BYTE_02 = 0x22, // read byte 02 from internal ram
    PS2_CTRL_CMD_READ_BYTE_03 = 0x23, // read byte 03 from internal ram
    PS2_CTRL_CMD_READ_BYTE_04 = 0x24, // read byte 04 from internal ram
    PS2_CTRL_CMD_READ_BYTE_05 = 0x25, // read byte 05 from internal ram
    PS2_CTRL_CMD_READ_BYTE_06 = 0x26, // read byte 06 from internal ram
    PS2_CTRL_CMD_READ_BYTE_07 = 0x27, // read byte 07 from internal ram
    PS2_CTRL_CMD_READ_BYTE_08 = 0x28, // read byte 08 from internal ram
    PS2_CTRL_CMD_READ_BYTE_09 = 0x29, // read byte 09 from internal ram
    PS2_CTRL_CMD_READ_BYTE_0A = 0x2A, // read byte 0A from internal ram
    PS2_CTRL_CMD_READ_BYTE_0B = 0x2B, // read byte 0B from internal ram
    PS2_CTRL_CMD_READ_BYTE_0C = 0x2C, // read byte 0C from internal ram
    PS2_CTRL_CMD_READ_BYTE_0D = 0x2D, // read byte 0D from internal ram
    PS2_CTRL_CMD_READ_BYTE_0E = 0x2E, // read byte 0E from internal ram
    PS2_CTRL_CMD_READ_BYTE_0F = 0x2F, // read byte 0F from internal ram
    PS2_CTRL_CMD_READ_BYTE_11 = 0x31, // read byte 11 from internal ram
    PS2_CTRL_CMD_READ_BYTE_12 = 0x32, // read byte 12 from internal ram
    PS2_CTRL_CMD_READ_BYTE_13 = 0x33, // read byte 13 from internal ram
    PS2_CTRL_CMD_READ_BYTE_14 = 0x34, // read byte 14 from internal ram
    PS2_CTRL_CMD_READ_BYTE_15 = 0x35, // read byte 15 from internal ram
    PS2_CTRL_CMD_READ_BYTE_16 = 0x36, // read byte 16 from internal ram
    PS2_CTRL_CMD_READ_BYTE_17 = 0x37, // read byte 17 from internal ram
    PS2_CTRL_CMD_READ_BYTE_18 = 0x38, // read byte 18 from internal ram
    PS2_CTRL_CMD_READ_BYTE_19 = 0x39, // read byte 19 from internal ram
    PS2_CTRL_CMD_READ_BYTE_1A = 0x3A, // read byte 1A from internal ram
    PS2_CTRL_CMD_READ_BYTE_1B = 0x3B, // read byte 1B from internal ram
    PS2_CTRL_CMD_READ_BYTE_1C = 0x3C, // read byte 1C from internal ram
    PS2_CTRL_CMD_READ_BYTE_1D = 0x3D, // read byte 1D from internal ram
    PS2_CTRL_CMD_READ_BYTE_1E = 0x3E, // read byte 1E from internal ram
    PS2_CTRL_CMD_READ_BYTE_1F = 0x3F, // read byte 1F from internal ram
    PS2_CTRL_CMD_READ_IN  = 0xC0, // read controller input port
    PS2_CTRL_CMD_READ_OUT = 0xD0, // read controller output port
}ps2_ctrl_cmd_read;

typedef enum _ps2_ctrl_cmd_write{
    PS2_CTR6_CMD_WRITE_BYTE_0  = 0x60, // write next byte to byte 00 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_01 = 0x61, // write next byte to byte 01 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_02 = 0x62, // write next byte to byte 02 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_03 = 0x63, // write next byte to byte 03 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_04 = 0x64, // write next byte to byte 04 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_05 = 0x65, // write next byte to byte 05 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_06 = 0x66, // write next byte to byte 06 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_07 = 0x67, // write next byte to byte 07 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_08 = 0x68, // write next byte to byte 08 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_09 = 0x69, // write next byte to byte 09 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0A = 0x6A, // write next byte to byte 0A of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0B = 0x6B, // write next byte to byte 0B of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0C = 0x6C, // write next byte to byte 0C of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0D = 0x6D, // write next byte to byte 0D of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0E = 0x6E, // write next byte to byte 0E of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_0F = 0x6F, // write next byte to byte 0F of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_11 = 0x71, // write next byte to byte 11 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_12 = 0x72, // write next byte to byte 12 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_13 = 0x73, // write next byte to byte 13 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_14 = 0x74, // write next byte to byte 14 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_15 = 0x75, // write next byte to byte 15 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_16 = 0x76, // write next byte to byte 16 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_17 = 0x77, // write next byte to byte 17 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_18 = 0x78, // write next byte to byte 18 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_19 = 0x79, // write next byte to byte 19 of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1A = 0x7A, // write next byte to byte 1A of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1B = 0x7B, // write next byte to byte 1B of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1C = 0x7C, // write next byte to byte 1C of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1D = 0x7D, // write next byte to byte 1D of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1E = 0x7E, // write next byte to byte 1E of internal ram
    PS2_CTRL_CMD_WRITE_BYTE_1F = 0x7F, // write next byte to byte 1F of internal ram
    PS2_CTRL_CMD_WRITE_OUT     = 0xD1, // write next byte to controller output port (need to check if output buffer empty first)
    PS2_CTRL_CMD_WRITE_OUT_FST = 0xD2, // write next byte to first PS2 output buffer (only if 2 PS/2 ports supported)
    PS2_CTRL_CMD_WRITE_OUT_SND = 0xD3, // write next byte to second PS2 output buffer (only if 2 PS/2 ports supported)
    PS2_CTRL_CMD_WRITE_IN_SND  = 0xD4, // write next byte to second PS2 intput buffer (only if 2 PS/2 ports supported)
}ps2_ctrl_cmd_write;

typedef enum _ps2_ctrl_cmd{
    PS2_CTRL_CMD_DSBL_SND  = 0xA7, // disable second port
    PS2_CTRL_CMD_DSBL_FST  = 0xAD, // disable first port
    PS2_CTRL_CMD_ENBL_SND  = 0xA8, // enable second port
    PS2_CTRL_CMD_ENBL_FST  = 0xAE, // enable first port
    PS2_CTRL_CMD_TEST_FST  = 0xAB, // test first port
    PS2_CTRL_CMD_TEST_SND  = 0xA9, // test second port
    PS2_CTRL_CMD_TEST_CTRL = 0xAA, // test the controller
    PS2_CTRL_CMD_DIAG_DUMP = 0xAC, // diagnostic dump

    PS2_CTRL_CMD_TEST_CTRL_FAILED = 0xFC, // controller test failed
    PS2_CTRL_CMD_TEST_CTRL_PASSED = 0x55, // controller test passed
    PS2_CTRL_CMD_TEST_FST_FAILED  = 0x01, // first port test failed
    PS2_CTRL_CMD_TEST_FST_PASSED  = 0x00, // first port test failed
    PS2_CTRL_CMD_TEST_SND_FAILED  = 0x01, // second port test failed
    PS2_CTRL_CMD_TEST_SND_PASSED  = 0x00, // second port test failed
}ps2_ctrl_cmd;

/**
 * Read the controller config byte
 * @return The config byte
*/
uint8_t ps2_read_ctrl_cfg_byte();

/**
 * Wait for a signal
*/
void ps2_wait();

/**
 * Clear a config bit
 * @param config The controller config byte
 * @param mask The bit to clear
*/
void ps2_ctrl_conf_clear_bit(uint8_t* config, ps2_ctrl_cfg_byte mask);

/**
 * Set a config bit
 * @param config The controller config byte
 * @param mask The bit to set
*/
void ps2_ctrl_conf_set_bit(uint8_t* config, ps2_ctrl_cfg_byte mask);

/**
 * Check a config bit
 * @param config The controller config byte
 * @param mask The bit to check
 * @return 1 if the bit was set, 0 otherwise
*/
uint8_t ps2_ctrl_conf_check_bit(uint8_t* config, ps2_ctrl_cfg_byte mask);

/**
 * Set the controller configuration byte
 * @param newConf The new configuration byte
*/
void ps2_set_ctrl_conf_byte(uint8_t newConf);

/**
 * Init the controller configuration byte and test if it can be a dual channel
*/
void ps2_init_ctrl_conf_byte();

/**
 * Initiate the ps2 controller routine
 * @return 0 if something went wrong, 1 otherwise
*/
uint8_t ps2_routine();

#endif //PS2_CONTROLLER_H
