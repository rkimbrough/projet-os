#ifndef PS2_LAYOUT
#define PS2_LAYOUT 1

#include <stdint.h>

typedef struct _ps2_layout
{
	char *description;
	char *from_sc[2];		//0 -> normal, 1 -> maj
	uint8_t caps_is_maj;	//1 si CapsLock permet de passer de la position 1 à
							//2 (comme sur AZERTY) ou non (comme sur QWERTY)
} ps2_layout;

#define N_LAYOUTS 1
typedef enum _ps2_layout_id
{
	PS2_QWERTY
} ps2_layout_id;

extern ps2_layout ps2_layouts_available[N_LAYOUTS];

#endif //PS2_LAYOUT
