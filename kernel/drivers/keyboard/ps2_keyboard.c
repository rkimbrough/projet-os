#include "ps2_keyboard.h"
#include "ps2/ps2_controller.h"

#include <kernel/interrupt.h>
#include <kernel/portio.h>
#include <kernel/tty.h>
#include <kernel/msg.h>
#include <kernel/commands.h>

#include <stdint.h>
#include <stdbool.h>

//-- Statut du driver --
typedef enum _ps2_kbd_cmdq_status
{
	PS2_KBD_CMDQ_FREE,
	PS2_KBD_CMDQ_ACK,
	PS2_KBD_CMDQ_ECHO,
	PS2_KBD_CMDQ_RESEND,
	PS2_KBD_CMDQ_SELF_TEST
} ps2_kbd_cmdq_status;

volatile sc_t ps2_current_sc;
static volatile ps2_kbd_cmdq_status pending;
static cmdq cmd_queue;

//-- Modifier le scan code en cours de lecture --
inline void ps2_reset_sc()
{
	ps2_current_sc.size = 0;
}

inline void ps2_add_to_sc(uint8_t code)
{
	ps2_current_sc.code[ps2_current_sc.size++] = code;
}

//-- Envoyer une commande à l'encodeur PS/2 --
void ps2_kbd_send_cmd(cmd_data_t cmd)
{
	switch (cmd)
	{
	case PS2_KBD_RESEND:
		pending = PS2_KBD_CMDQ_RESEND;
		break;
	case PS2_KBD_ECHO:
		pending = PS2_KBD_CMDQ_ECHO;
		break;
	case PS2_KBD_SELF_TEST:
		pending = PS2_KBD_CMDQ_SELF_TEST;
		break;
	default:
		pending = PS2_KBD_CMDQ_ACK;
	}

	outb(PS2_ENCODER_PORT, cmd);
}

//-- ISR --
static inline bool expected_answer(uint8_t sc)
{
	return (
		(sc == PS2_KBD_ACK && pending == PS2_KBD_CMDQ_ACK)
		|| (sc == PS2_KBD_OHCE && pending == PS2_KBD_CMDQ_ECHO)
		|| (sc == PS2_KBD_PASSED && pending == PS2_KBD_CMDQ_SELF_TEST)
	);
}

__attribute__((interrupt))
void ps2_isr(interrupt_frame *frame __attribute__((unused)))
{
	uint8_t sc = inb(PS2_ENCODER_PORT);
	if (sc == PS2_KBD_DNESER)
		pending = PS2_KBD_CMDQ_FREE;
	else if (expected_answer(sc))
	{
		pending = PS2_KBD_CMDQ_FREE;
		pop_cmd(&cmd_queue);
	}
	else if (pending == PS2_KBD_CMDQ_SELF_TEST && (sc == PS2_KBD_FAIL1 || sc == PS2_KBD_FAIL2))
	{
		//panic
	}
	else if (sc == PS2_KBD_ERROR1 || sc == PS2_KBD_ERROR2)
	{
		//panic
	}
	else if (sc == 0xE0)
	{
		ps2_add_to_sc(0xE0);
	}
	else
	{
		ps2_add_to_sc(sc);
		push_msg(ps2_current_sc);
		ps2_reset_sc();
	}

	send_EOI(KBD_INT);
}

//-- Fonctions du driver : fonctions générales --
void ps2_kbd_driver_init()
{
	ps2_reset_sc();
	pending = PS2_KBD_CMDQ_FREE;

	set_interrupt(IRQ_START + KBD_INT, &ps2_isr);
	enable_irq(KBD_INT);
	term_writestring("Keyboard initialized.\n");
}

void ps2_kbd_driver_update()
{
	if (cmd_queue.size > 0 && pending == PS2_KBD_CMDQ_FREE)
	{
		cmd_t cmd = front_cmd(&cmd_queue);
		cmd.routine(cmd.data);
	}
}

const char *ps2_kbd_get_device_name(void *device)
{
	return ((keyboard *)device)->name;
}

//-- Fonctions du driver : gestion du clavier --
keyboard ps2_keyboard = { "PS/2", ps2_layouts_available, 0, 0 };
keyboard *ps2_add_keyboard(const char *)
{
	//Il faudra malloc pour faire ça bien...
	return &ps2_keyboard;
}

void ps2_toggle_lock(keyboard *k, lock l)
{
	k->lock_status ^= 1 << l;
	queue_cmd(&cmd_queue, PS2_KBD_SET_LEDS, ps2_kbd_send_cmd);
	queue_cmd(&cmd_queue, k->lock_status, ps2_kbd_send_cmd);
}

uint8_t ps2_check_lock(keyboard *k, lock l)
{
	return k->lock_status >> l & 1;
}

void ps2_set_held(keyboard *k, held h)
{
	k->held_status |= 1 << h;
}

void ps2_clear_held(keyboard *k, held h)
{
	k->held_status &= ~(1 << h);
}

uint8_t ps2_check_held(keyboard *k, held h)
{
	return k->held_status >> h & 1;
}

inline uint8_t ps2_is_alpha_sc(keyboard *k, uint8_t sc)
{
	return ('a' <= k->layout->from_sc[0][sc] && k->layout->from_sc[0][sc] <= 'z');
}

static uint8_t sc_row[256] = {
	0,	1,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	2,	3,
	3,	3,	3,	3,	3,	3,	3,	3,	3,	3,	3,	3,	3,	6,	4,	4,
	4,	4,	4,	4,	4,	4,	4,	4,	4,	2,	5,	4,	5,	5,	5,	5,
	5,	5,	5,	5,	5,	5,	5,	2,	6,	6,	4,	1,	1,	1,	1,	1,
	1,	1,	1,	1,	1,	2,	1,	3,	3,	3,	2,	4,	4,	4,	3,	5,
	5,	5,	6,	6,	0,	0,	0,	1,	1,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	5,	6,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	2,	0,	0,	6,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	2,	5,	2,	0,	6,	0,	6,	0,	3,
	6,	3,	2,	3,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0
};

static uint8_t sc_col[256] = {
	0,	1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	1,
	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	1,	2,	3,
	4,	5,	6,	7,	8,	9,	10,	11,	12,	1,	1,	13,	2,	3,	4,	5,
	6,	7,	8,	9,	10,	11,	12,	20,	3,	4,	1,	2,	3,	4,	5,	6,
	7,	8,	9,	10,	11,	18,	15,	18,	19,	20,	21,	14,	15,	16,	21,	14,
	15,	16,	12,	13,	0,	0,	0,	12,	13,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	17,	8,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	19,	0,	0,	5,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	16,	13,	17,	0,	9,	0,	11,	0,	16,
	10,	17,	15,	15,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,
	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0
};

key_info ps2_code_to_key(keyboard *k, sc_t sc)
{
	uint8_t effective_sc = sc.code[sc.size - 1];
	bool release = (effective_sc >= 0x80);
	if (release)
		effective_sc -= 0x80;
	if (sc.size > 1)
		effective_sc += 0x80;

	switch (effective_sc)
	{
	case PS2_SCROLL:
		if (!release)
			ps2_toggle_lock(k, SCROLL);
		break;
	case PS2_NUMBER:
		if (!release)
			ps2_toggle_lock(k, NUMBER);
		break;
	case PS2_CAPS:
		if (!release)
			ps2_toggle_lock(k, CAPS);
		break;
	case PS2_LEFT_SHIFT:
		if (release)
			ps2_clear_held(k, LEFT_SHIFT);
		else
			ps2_set_held(k, LEFT_SHIFT);
		break;
	case PS2_RIGHT_SHIFT:
		if (release)
			ps2_clear_held(k, RIGHT_SHIFT);
		else
			ps2_set_held(k, RIGHT_SHIFT);
		break;
	case PS2_LEFT_CONTROL:
		if (release)
			ps2_clear_held(k, LEFT_CONTROL);
		else
			ps2_set_held(k, LEFT_CONTROL);
		break;
	case PS2_RIGHT_CONTROL:
		if (release)
			ps2_clear_held(k, RIGHT_CONTROL);
		else
			ps2_set_held(k, RIGHT_CONTROL);
		break;
	case PS2_LEFT_ALT:
		if (release)
			ps2_clear_held(k, LEFT_ALT);
		else
			ps2_set_held(k, LEFT_ALT);
		break;
	case PS2_RIGHT_ALT:
		if (release)
			ps2_clear_held(k, RIGHT_ALT);
		else
			ps2_set_held(k, RIGHT_ALT);
		break;
	}

	uint8_t maj = (ps2_check_held(k, LEFT_SHIFT) | ps2_check_held(k, RIGHT_SHIFT));
	if (ps2_check_lock(k, CAPS) && (k->layout->caps_is_maj || ps2_is_alpha_sc(k, effective_sc)))
		maj ^= 1;

	key_info kinf;
	kinf.chr = k->layout->from_sc[maj][effective_sc];
	kinf.kcode_row = sc_row[effective_sc];
	kinf.kcode_col = sc_col[effective_sc];
	kinf.release = release;
	return kinf;
}

//-- Driver --
kbd_driver ps2_kbd_driver = {
	{
		"PS/2 Keyboard",
		&ps2_kbd_driver_init,
		&ps2_kbd_driver_update,
		&ps2_kbd_get_device_name
	},
	&ps2_add_keyboard,
	&ps2_toggle_lock,
	&ps2_check_lock,
	&ps2_check_held,
	&ps2_code_to_key
};
