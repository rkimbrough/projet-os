#ifndef PS2_KEYBOARD_H
#define PS2_KEYBOARD_H 1

#include "ps2_layout.h"

#include <kernel/keyboard.h>

//-- Structures --
struct _keyboard
{
	char *name;
	ps2_layout *layout;

	//Registres de status
	uint8_t lock_status;	//	|0					|C	|N	|S	|
	uint8_t held_status;	//	|0		|RA	|LA	|RC	|LC	|RS	|LS	|
};

//-- Constantes pour les touches spéciales --
typedef enum _ps2_lock_keys
{
	PS2_SCROLL	= 0x46,
	PS2_NUMBER	= 0x45,
	PS2_CAPS 	= 0x3A
} ps2_lock_keys;

typedef enum _ps2_special_keys
{
	PS2_LEFT_SHIFT		= 0x2A,
	PS2_RIGHT_SHIFT		= 0x36,
	PS2_LEFT_CONTROL	= 0x1D,
	PS2_RIGHT_CONTROL	= 0x9D,
	PS2_LEFT_ALT		= 0x38,
	PS2_RIGHT_ALT		= 0xB8
} ps2_special_keys;

//-- Constantes pour les commandes --
typedef enum _ps2_kbd_cmd
{
	PS2_KBD_SET_LEDS	= 0xED,
	PS2_KBD_ECHO		= 0xEE,
	PS2_KBD_CMD_SC		= 0xF0,
	PS2_KBD_ID			= 0xF2,
	PS2_KBD_TYPEMATIC	= 0xF3,
	PS2_KBD_ENABLE		= 0xF4,
	PS2_KBD_DISABLE		= 0xF5,
	PS2_KBD_DEFAULT		= 0xF6,
	PS2_KBD_RESEND		= 0xFE,
	PS2_KBD_SELF_TEST	= 0xFF
} ps2_kbd_cmd;

typedef enum _ps2_kbd_answer
{
	PS2_KBD_ERROR1	= 0x00,
	PS2_KBD_PASSED	= 0xAA,
	PS2_KBD_OHCE	= 0xEE,	//Simplement la réponse à ECHO
	PS2_KBD_ACK		= 0xFA,
	PS2_KBD_FAIL1	= 0xFC,
	PS2_KBD_FAIL2	= 0xFD,
	PS2_KBD_DNESER	= 0xFE, //RESEND, de la part du clavier
	PS2_KBD_ERROR2	= 0xFF
} ps2_kbd_answer;

#endif //PS2_KEYBOARD_H
