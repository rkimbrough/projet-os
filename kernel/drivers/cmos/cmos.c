#include "cmos.h"

#include <kernel/portio.h>
#include <kernel/interrupt.h>

#include <stdint.h>

uint8_t nmi_status;

uint8_t cmos_read_reg(cmos_register reg)
{
	outb(CMOS_SELECTOR, nmi_status | reg);
	return inb(CMOS_REGISTER);
}

void cmos_write_reg(cmos_register reg, uint8_t data)
{
	outb(CMOS_SELECTOR, nmi_status | reg);
	outb(CMOS_REGISTER, data);
}

void enable_nmi()
{
	nmi_status = 0;
	cmos_read_reg(CMOS_STATUS_A); //Reading any register to inform the system
}

void disable_nmi()
{
	nmi_status = 1 << 7;
	cmos_read_reg(CMOS_STATUS_A); //Reading any register to inform the system
}
