#ifndef CMOS_H
#define CMOS_H 1

#include <stdint.h>

#ifdef __arch_i386__
#define CMOS_SELECTOR 0x70
#define CMOS_REGISTER 0x71
#endif

typedef enum _cmos_register
{
	CMOS_SECONDS	= 0x00,
	CMOS_MINUTES	= 0x02,
	CMOS_HOURS		= 0x04,
	CMOS_DAY		= 0x07,
	CMOS_MONTH		= 0x08,
	CMOS_YEAR		= 0x09,
	CMOS_STATUS_A	= 0x0A,
	CMOS_STATUS_B	= 0x0B,
	CMOS_INT_INFO	= 0x0C,
	CMOS_FLOP_INFO	= 0x10
} cmos_register;

typedef enum _cmos_status_b_flags
{
	CMOS_B_24H		= 1 << 1,
	CMOS_B_BIN		= 1 << 2,
	CMOS_B_UPD_INT	= 1 << 4,
	CMOS_B_TICK_INT	= 1 << 6
} cmos_status_b_flags;

uint8_t cmos_read_reg(cmos_register reg);
void cmos_write_reg(cmos_register reg, uint8_t data);

#endif
