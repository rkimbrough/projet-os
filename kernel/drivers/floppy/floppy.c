#include "floppy.h"
#include <kernel/portio.h>

// Le disque courant
static floppy_drive _current_drive = FLPY_DRIVE_0;

// Est instancié quand l'interruption se lance
static volatile bool _floppy_irq = false;

void flpy_init_dma(){
	outb (0x0a,0x06);	//mask dma channel 2
	outb (0xd8,0xff);	//reset master flip-flop
	outb (0x04, 0);     //address=0x1000 
	outb (0x04, 0x10);
	outb (0xd8, 0xff);  //reset master flip-flop
	outb (0x05, 0xff);  //count to 0x23ff (number of bytes in a 3.5" floppy disk track)
	outb (0x05, 0x23);
	outb (0x80, 0);     //external page register = 0
	outb (0x0a, 0x02);  //unmask dma channel 2
}

void flpy_dma_read(){
	outb (0x0a, 0x06); //mask dma channel 2
	outb (0x0b, 0x56); //single transfer, address increment, autoinit, read, channel 2
	outb (0x0a, 0x02); //unmask dma channel 2
}

void flpy_dma_write(){
	outb (0x0a, 0x06); //mask dma channel 2
	outb (0x0b, 0x5a); //single transfer, address increment, autoinit, write, channel 2
	outb (0x0a, 0x02); //unmask dma channel 2
}

void flpy_write_dor(uint8_t value){
    outb(FLPY_DIGITAL_OUTPUT_REGISTER, value);
}

uint8_t flpy_read_status(){
    return inb(FLPY_MAIN_STATUS_REGISTER);
}

void flpy_send_command(uint8_t cmd){
    // wait until data register is ready
	for (int i = 0; i < 500; i++ ){
		if ( flpy_read_status() & FLPY_MSR_MASK_DATAREG ){
			return outb(FLPY_DATA_FIFO, cmd);
        }
    }
}

uint8_t flpy_read_data(){
    // wait until data register is ready
	for (int i = 0; i < 500; i++ ){
		if ( flpy_read_status () & FLPY_MSR_MASK_DATAREG ){
			return inb (FLPY_DATA_FIFO);
        }
    }
    // error
    return 0x80;
}

void flpy_write_ccr(uint8_t val){
	outb (FLPY_CONFIGURATION_CONTROL_REGISTER, val);
}

void flpy_read_sector_imp(uint8_t head, uint8_t track, uint8_t sector){
 
	uint32_t st0, cyl;
 
	// set the DMA for read transfer
	flpy_dma_read();
 
	// read in a sector
	flpy_send_command(
		FLPY_CMD_READ_DATA | FLPY_CMD_EXT_MULTITRACK |
		FLPY_CMD_EXT_SKIP | FLPY_CMD_EXT_DENSITY);
	flpy_send_command(head << 2 | _current_drive);
	flpy_send_command(track);
	flpy_send_command(head);
	flpy_send_command(sector);
	flpy_send_command(FLPY_SECTOR_NB_512);
	flpy_send_command(
		( ( sector + 1 ) >= FLPY_SECTORS_PER_TRACK )
			? FLPY_SECTORS_PER_TRACK : sector + 1 );
	flpy_send_command(FLPY_GAP3_LENGTH_3_5);
	flpy_send_command(0xff);
 
	// wait for irq
	flpy_wait_irq();
 
	// read status info
	for (int j=0; j<7; j++)
		flpy_read_data();
 
	// let FDC know we handled interrupt
	flpy_check_int(&st0,&cyl);
}

void flpy_write_sector_imp(uint8_t head, uint8_t track, uint8_t sector){
 
	uint32_t st0, cyl;
 
	// set the DMA for write transfer
	flpy_dma_write();
 
	// read in a sector
	flpy_send_command(
		FLPY_CMD_WRITE_DATA | FLPY_CMD_EXT_MULTITRACK |
		FLPY_CMD_EXT_SKIP | FLPY_CMD_EXT_DENSITY);
	flpy_send_command(head << 2 | _current_drive);
	flpy_send_command(track);
	flpy_send_command(head);
	flpy_send_command(sector);
	flpy_send_command(FLPY_SECTOR_NB_512);
	flpy_send_command(
		( ( sector + 1 ) >= FLPY_SECTORS_PER_TRACK )
			? FLPY_SECTORS_PER_TRACK : sector + 1 );
	flpy_send_command(FLPY_GAP3_LENGTH_3_5);
	flpy_send_command(0xff);
 
	// wait for irq
	flpy_wait_irq();
 
	// read status info
	for (int j=0; j<7; j++)
		flpy_read_data();
 
	// let FDC know we handled interrupt
	flpy_check_int(&st0,&cyl);
}

void flpy_lba_to_chs(uint8_t lba, uint8_t* head, uint8_t* track, uint8_t* sector){
   *head = ( lba % ( FLPY_SECTORS_PER_TRACK * 2 ) ) / ( FLPY_SECTORS_PER_TRACK );
   *track = lba / ( FLPY_SECTORS_PER_TRACK * 2 );
   *sector = lba % FLPY_SECTORS_PER_TRACK + 1;
}


uint8_t* flpy_read_sector(uint8_t sectorLBA){
	// term_writestring("begin read\n");
	if (_current_drive >= 4)
		return 0;

	// convert LBA sector to CHS
	uint8_t head=0, track=0, sector=1;
	flpy_lba_to_chs(sectorLBA, &head, &track, &sector);
	// term_writestring("conversion done\n");

	// turn motor on and seek to track
	flpy_control_motor(true);
	// term_writestring("motor on\n");
	if (flpy_seek(track, head) != 0)
		return 0;
	// term_writestring("seek done\n");

	// read sector and turn motor off
	flpy_read_sector_imp(head, track, sector);
	// term_writestring("read done\n");
	flpy_control_motor(false);
	// term_writestring("motor off\n");

	return (uint8_t*) FLPY_DMA_BUFFER;
}

void flpy_write_sector(uint8_t sectorLBA, uint8_t* data, int size){
	// term_writestring("begin read\n");
	if (_current_drive >= 4)
		return;

	// convert LBA sector to CHS
	uint8_t head=0, track=0, sector=1;
	flpy_lba_to_chs(sectorLBA, &head, &track, &sector);
	// term_writestring("conversion done\n");

	// turn motor on and seek to track
	flpy_control_motor(true);
	// term_writestring("motor on\n");
	if (flpy_seek(track, head) != 0)
		return;
	// term_writestring("seek done\n");

	// write sector and turn motor off
	flpy_write_sector_imp(head, track, sector);
	// term_writestring("read done\n");

	// Write data to the FDC's DMA buffer
    uint8_t* dmaBuffer = (uint8_t*)FLPY_DMA_BUFFER;
    for (int i = 0; i < size; i++) {
        dmaBuffer[i] = data[i];
    }

	flpy_control_motor(false);
	// term_writestring("motor off\n");
}

__attribute__((interrupt))
void flpy_isr(interrupt_frame *frame __attribute__((unused))){
    _floppy_irq = true;
    send_EOI(FLPY_INT);
}

void flpy_wait_irq(){
    while(!_floppy_irq){
        // term_writestring("Test.\n");
    };
    // reset the irq handler
    _floppy_irq = false;
}

void flpy_check_int(uint32_t *st0, uint32_t *cyl){
    flpy_send_command(FLPY_CMD_SENSE_INTERRUPT);
    *st0 = flpy_read_data();
    *cyl = flpy_read_data();
}

void flpy_drive_data(uint32_t stepr, uint32_t loadt, uint32_t unloadt, bool dma){
	uint32_t data = 0;
	flpy_send_command(FLPY_CMD_SPECIFY);

	data = ( (stepr & 0xf) << 4) | (unloadt & 0xf);
	flpy_send_command(data);
 
	data = (loadt) << 1 | ((dma==true) ? 1 : 0);
	flpy_send_command(data);
}

int flpy_calibrate(uint32_t drive){
	uint32_t st0, cyl;
 
	if (drive >= 4)
		return -2;
 
	// turn on the motor
	flpy_control_motor(true);

    // test 10 times then abort
	for (int i = 0; i < 10; i++) {
 
		// send command
		flpy_send_command(FLPY_CMD_RECALIBRATE );
		flpy_send_command(drive);
		flpy_wait_irq();
		flpy_check_int(&st0, &cyl);
 
		// did we fine cylinder 0? if so, we are done
		if (!cyl){
			flpy_control_motor(false);
			return 0;
		}
	}
 
	flpy_control_motor (false);
	return -1;
}

void flpy_control_motor(bool b){
    // select the correct motor
    uint32_t motor;
    switch(_current_drive){
        case FLPY_DRIVE_0: {
            motor = FLPY_DOR_MASK_DRIVE0_MOTOR;
            break;
        }
        case FLPY_DRIVE_1: {
            motor = FLPY_DOR_MASK_DRIVE1_MOTOR;
            break;
        }
        case FLPY_DRIVE_2: {
            motor = FLPY_DOR_MASK_DRIVE2_MOTOR;
            break;
        }
        case FLPY_DRIVE_3: {
            motor = FLPY_DOR_MASK_DRIVE3_MOTOR;
            break;
        }
        // invalid drive
        default: {
            term_writestring("Invalid drive !");
            return;
        }
    }

    // activate or deactivate the motor
    if(b){
        flpy_write_dor(_current_drive | motor | FLPY_DOR_MASK_RESET | FLPY_DOR_MASK_DMA);
    } else {
        flpy_write_dor(FLPY_DOR_MASK_RESET);
    }

    // wait for the motors to spin up or turn off
    // TODO: implement sleep function 
}

int flpy_seek(uint32_t cyl, uint32_t head){
	uint32_t st0, cyl0;
 
    // check if the drive is correct
	if (_current_drive >= FLPY_MAX_DRIVES)
		return -1;
 
    // test 10 times then abort
	for (int i = 0; i < 10; i++ ) {
		// send the command
		flpy_send_command(FLPY_CMD_SEEK);
		flpy_send_command((head) << 2 | _current_drive);
		flpy_send_command(cyl);
 
		// wait for the results phase IRQ
		flpy_wait_irq();
		flpy_check_int(&st0,&cyl0);
 
		// found the cylinder?
		if ( cyl0 == cyl)
			return 0;
	}
 
	return -1;
}

void flpy_disable_controller(){
	flpy_write_dor(0);
}

void flpy_enable_controller(){
	flpy_write_dor(FLPY_DOR_MASK_RESET | FLPY_DOR_MASK_DMA);
}

void flpy_reset(){
	uint32_t st0, cyl;

	_floppy_irq = false; 	// This will prevent the FDC from being faster than us!
  
	// reset the controller
	flpy_disable_controller();
	flpy_enable_controller();
	flpy_wait_irq();
 
	// send CHECK_INT/SENSE INTERRUPT command to all drives
	for (int i=0; i<FLPY_MAX_DRIVES; i++)
		flpy_check_int(&st0,&cyl);    
 
	// transfer speed 500kb/s
	flpy_write_ccr(FLPY_CCR_500_KBPS);
 
	// pass mechanical drive info. steprate=3ms, unload time=240ms, load time=16ms
	flpy_drive_data(3, 16, 240, true);
 
	// calibrate the disk
	flpy_calibrate(_current_drive);

    // term_writestring("FDC reset\n");
}

void flpy_init(){
    // set the interrupt handler
	// TODO: change 0x20 by IRQ_START
    set_interrupt(0x20 + FLPY_INT, flpy_isr);
	enable_irq(FLPY_INT);

    // initialize the DMA for FDC
	flpy_init_dma();
	// reset the fdc
	flpy_reset();
    
	// set drive information
	flpy_drive_data(13, 1, 0xf, true);

    term_writestring("Floppy Drive Controller initialized.\n");
}

void flpy_update(){
    // ???
}

const char* flpy_get_device_name(void* device){
    return ((floppy *)device)->name;
}

// Driver
flpy_driver dma_irq_flpy_driver = {
    {
        "DMA/IRQ Floppy",
        &flpy_init,
        &flpy_update,
        &flpy_get_device_name
    },
	&flpy_read_sector,
	&flpy_write_sector,
};
