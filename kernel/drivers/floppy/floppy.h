/**
Le controlleur floppy (FDC) possède 9 registres pouvant être accédé par les IO ports 0x3F0 à 0x3F7

Le FDC est en mode DMA (Direct Memory Access)

Parmi les registres du FDC, on a:

Le Digital Output Register (DOR) -> En écriture seule, il permet de controller différentes fonctions du FDC
Le Main Status Register (MSR) -> Il contient les informations de statuts du FDC et des disques (FDDs). Avant de lire ou écrire dans un FDD, il faut checker le statut du FDC
Le Data Register -> Toutes les données sont lues et écrites depuis le data register

*/

#pragma once

#include <kernel/floppy.h>

#include <kernel/interrupt.h>
#include <kernel/portio.h>
#include <kernel/tty.h>
#include <kernel/msg.h>
#include <kernel/commands.h>

#include <stdint.h>
#include <stdbool.h>

// Constantes pour accéder aux registres du floppy 
typedef enum _floppy_registers{
   FLPY_STATUS_REGISTER_A                = 0x3F0, // read-only
   FLPY_STATUS_REGISTER_B                = 0x3F1, // read-only
   FLPY_DIGITAL_OUTPUT_REGISTER          = 0x3F2,
   FLPY_TAPE_DRIVE_REGISTER              = 0x3F3,
   FLPY_MAIN_STATUS_REGISTER             = 0x3F4, // read-only
   FLPY_DATARATE_SELECT_REGISTER         = 0x3F4, // write-only
   FLPY_DATA_FIFO                        = 0x3F5,
   FLPY_DIGITAL_INPUT_REGISTER           = 0x3F7, // read-only
   FLPY_CONFIGURATION_CONTROL_REGISTER   = 0x3F7  // write-only
} floppy_registers;

// Constantes pour le mask du Digital Output Register (DOR)
typedef enum _floppy_dor_mask {
	FLPY_DOR_MASK_DRIVE0			   =	0,	   //00000000
	FLPY_DOR_MASK_DRIVE1			   =	1,	   //00000001
	FLPY_DOR_MASK_DRIVE2			   =	2,	   //00000010
	FLPY_DOR_MASK_DRIVE3			   =	3,	   //00000011
	FLPY_DOR_MASK_RESET			   =	4,	   //00000100
	FLPY_DOR_MASK_DMA			      =	8,	   //00001000
	FLPY_DOR_MASK_DRIVE0_MOTOR		=	16,	//00010000
	FLPY_DOR_MASK_DRIVE1_MOTOR		=	32,	//00100000
	FLPY_DOR_MASK_DRIVE2_MOTOR		=	64,	//01000000
	FLPY_DOR_MASK_DRIVE3_MOTOR		=	128	//10000000
} floppy_dor_mask;

// Constantes pour le mask du Main Status Register (MSR)
typedef enum _floppy_msr_mask {
	FLPY_MSR_MASK_DRIVE1_POS_MODE	=	1,	   //00000001
	FLPY_MSR_MASK_DRIVE2_POS_MODE	=	2,	   //00000010
	FLPY_MSR_MASK_DRIVE3_POS_MODE	=	4,	   //00000100
	FLPY_MSR_MASK_DRIVE4_POS_MODE	=	8,	   //00001000
	FLPY_MSR_MASK_BUSY			   =	16,	//00010000
	FLPY_MSR_MASK_DMA			      =	32,	//00100000
	FLPY_MSR_MASK_DATAIO			   =	64, 	//01000000
	FLPY_MSR_MASK_DATAREG		   =	128	//10000000
} floppy_msr_mask;

// Constantes pour le mask du Configuration Control Register (CCR)
typedef enum _floppy_ccr_mask {
   FLPY_CCR_500_KBPS	=	0,	   //00
   FLPY_CCR_250_KBPS	=	1,	   //01
   FLPY_CCR_300_KBPS	=	2,	   //10
   FLPY_CCR_1_MBPS	=	3	   //11
} floppy_ccr_mask;

// Initialise le DMA pour le floppy
void flpy_init_dma();

// Prépare le DMA pour la lecture du floppy
void flpy_dma_read();

// Prépare le DMA pour l'écriture du floppy
void flpy_dma_write();

// Routine pour écrire dans le DOR
void flpy_write_dor(uint8_t value);

// Routine pour lire le statut du FDC
uint8_t flpy_read_status();

// Envoie une commande au data register
void flpy_send_command(uint8_t cmd);

// Lit la donnée du data register
// Si la commande était invalide, renvoie 0x80
uint8_t flpy_read_data();

// Écrit sur le Configuration Control Register (CCR)
// Le CCR gère les vitesses d'accès aux données
void flpy_write_ccr (uint8_t val);

// Enumeration des comandes pour le FDC
typedef enum _floppy_cmd {
	FLPY_CMD_READ_TRACK =                 2,	// generates IRQ6
   FLPY_CMD_SPECIFY =                    3,      // * set drive parameters
   FLPY_CMD_SENSE_DRIVE_STATUS =         4,
   FLPY_CMD_WRITE_DATA =                 5,      // * write to the disk
   FLPY_CMD_READ_DATA =                  6,      // * read from the disk
   FLPY_CMD_RECALIBRATE =                7,      // * seek to cylinder 0
   FLPY_CMD_SENSE_INTERRUPT =            8,      // * ack IRQ6, get status of last command
   FLPY_CMD_WRITE_DELETED_DATA =         9,
   FLPY_CMD_READ_ID =                    10,	// generates IRQ6
   FLPY_CMD_READ_DELETED_DATA =          12,
   FLPY_CMD_FORMAT_TRACK =               13,     // *
   FLPY_CMD_DUMPREG =                    14,
   FLPY_CMD_SEEK =                       15,     // * seek both heads to cylinder X
   FLPY_CMD_VERSION =                    16,	// * used during initialization, once
   FLPY_CMD_SCAN_EQUAL =                 17,
   FLPY_CMD_PERPENDICULAR_MODE =         18,	// * used during initialization, once, maybe
   FLPY_CMD_CONFIGURE =                  19,     // * set controller parameters
   FLPY_CMD_LOCK =                       20,     // * protect controller params from a reset
   FLPY_CMD_VERIFY =                     22,
   FLPY_CMD_SCAN_LOW_OR_EQUAL =          25,
   FLPY_CMD_SCAN_HIGH_OR_EQUAL =         29
}floppy_cmd;

typedef enum _floppy_cmd_ext {
	FLPY_CMD_EXT_SKIP	=	0x20,	//00100000
	FLPY_CMD_EXT_DENSITY	=	0x40,	//01000000
	FLPY_CMD_EXT_MULTITRACK	=	0x80	//10000000
} floppy_cmd_ext;

// l'Espace entre les secteurs sur le disque physique
typedef enum _floppy_gap3_length {
	FLPY_GAP3_LENGTH_STD = 42,
	FLPY_GAP3_LENGTH_5_14= 32,
	FLPY_GAP3_LENGTH_3_5= 27
} floppy_gap3_length;

// La taille des octets par secteurs
typedef enum _floppy_sector_nb {
	FLPY_SECTOR_NB_128	=	0,
	FLPY_SECTOR_NB_256	=	1,
	FLPY_SECTOR_NB_512	=	2,
	FLPY_SECTOR_NB_1024	=	4
} floppy_sector_nb;

// Lit un secteur
void flpy_read_sector_imp (uint8_t head, uint8_t track, uint8_t sector);
uint8_t* flpy_read_sector(uint8_t sectorLBA);

// Écrit dans un secteur
void flpy_write_sector_imp (uint8_t head, uint8_t track, uint8_t sector);
void flpy_write_sector(uint8_t sectorLBA, uint8_t* data, int size);

// Convertis du LBA (Linear Block Adressing) en CHS (Cylinder/Head/Sector)
void flpy_lba_to_chs (uint8_t lba, uint8_t* head, uint8_t* track, uint8_t* sector);

// Le nombre de secteurs par tracks
#define FLPY_SECTORS_PER_TRACK 18

// Le nombre maximal de disques
#define FLPY_MAX_DRIVES 4

// Le DMA commence à 0x1000 et fini à 0x1000+64k
const int FLPY_DMA_BUFFER = 0x1000;

// les numéros des différents disques
typedef enum _floppy_drive {
   FLPY_DRIVE_0 = 0,
   FLPY_DRIVE_1 = 1,
   FLPY_DRIVE_2 = 2,
   FLPY_DRIVE_3 = 3
} floppy_drive;

// Le gestionnaire d'interruption pour le floppy
void flpy_isr(interrupt_frame *frame __attribute__((unused)));

// Attends l'interruption
void flpy_wait_irq();

// Check l'interruption
void flpy_check_int(uint32_t* st0, uint32_t* cyl);

// Envoie des informations au FDC concernant le FDD connecté
void flpy_drive_data(uint32_t stepr, uint32_t loadt, uint32_t unloadt, bool dma);

// Place la tête du cylindre en 0 et allume les moteurs pendant la commande
int flpy_calibrate(uint32_t drive);

// Allume et éteint les moteurs
void flpy_control_motor(bool b);

// Déplace la tête de lecture
int flpy_seek(uint32_t cyl, uint32_t head);

// Désactive le FDC
void flpy_disable_controller();

// Active le FDC
void flpy_enable_controller();

// Reset le FDC
void flpy_reset();

// Init le FDC
void flpy_init();

// Met à jour le FDC
void flpy_update();

// Renvoie le nom du controller
const char* flpy_get_device_name(void* device);
