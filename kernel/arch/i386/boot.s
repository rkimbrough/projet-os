.set		ALIGN, 1 << 0
.set		MEMINFO, 1 << 1
.set		FLAGS, ALIGN | MEMINFO
.set		MAGIC, 0x1BADB002	# ???
.set		CHECKSUM, -(MAGIC + FLAGS)

.section	.multiboot
.align		4
.long		MAGIC
.long		FLAGS
.long		CHECKSUM

.section	.bss
.align		16
stack_bottom:
.skip		16384	# Pile de 16KiB
stack_top: 

.section	.text
.global		_start
.type		_start, @function
_start:
# On peut initialiser des trucs très bas niveau ici
	mov		$stack_top, %esp

# Chargement du GDT
	call	create_gdt
	call	load_gdt

# Chargement de tout ce qui touche aux interrupt
	call	create_idt
	call	load_idt
	call	PIC_init
	call	ps2_routine

# TODO call les différents init pour le paging 
# But when shall we do this initialization ?
	call init_tables
	call enb_paging


# Trucs de plus haut niveau
	sti
# provoque une page fault
#	mov $16777216, %eax
#	mov (%eax), %ebx
# fin truc inutile
	call	_init
	call	kernel_main

# Boucle infinie, pas censé arriver à la fin
_loop:
	hlt
	jmp		_loop

.size		_start, . - _start
