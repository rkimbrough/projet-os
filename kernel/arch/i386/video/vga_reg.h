#ifndef VGA_REG_H
#define VGA_REG_H 1

#include <stdint.h>

//-- Registres externes --
#define MISC_OUT_WRITE 0x3C2
#define MISC_OUT_READ 0x3CC

typedef enum _misc_flags
{
	ADDR_OFFSET = 1 << 0
} misc_flags;

//-- Registres CRT --
#define ADDR_REG 0x3B4
#define DATA_REG 0x3B5

typedef enum _crt_reg
{
	CURSOR_START	= 0x0A,
	CURSOR_LOC_HI	= 0x0E,
	CURSOR_LOC_LO	= 0x0F
} crt_reg;

typedef enum _cursor_start_flags
{
	DISABLE_CURSOR = 1 << 5
} cursor_start_flags;

#endif //VGA_REG_H
