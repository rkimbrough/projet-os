#include "vga_reg.h"

#include <kernel/vga.h>
#include <kernel/portio.h>

#include <stdint.h>

#define DISPLAY_ADRESS 0xB8000

static display *vga_buffer;
static uint16_t po;

uint8_t crt_read(crt_reg reg)
{
	uint16_t addr_reg = ADDR_REG + po;
	uint16_t data_reg = DATA_REG + po;

	uint8_t bckp = inb(addr_reg);
	outb(addr_reg, reg);
	uint8_t data = inb(data_reg);
	outb(addr_reg, bckp);
	return data;
}

void crt_write(crt_reg reg, uint8_t data)
{
	uint16_t addr_reg = ADDR_REG + po;
	uint16_t data_reg = DATA_REG + po;

	uint8_t bckp = inb(addr_reg);
	outb(addr_reg, reg);
	outb(data_reg, data);
	outb(addr_reg, bckp);
}

void crt_set_flags(crt_reg reg, uint8_t flags)
{
	uint16_t addr_reg = ADDR_REG + po;
	uint16_t data_reg = DATA_REG + po;

	uint8_t bckp = inb(addr_reg);
	outb(addr_reg, reg);
	uint8_t data = inb(data_reg);
	outb(data_reg, data | flags);
	outb(addr_reg, bckp);
}

void crt_clear_flags(crt_reg reg, uint8_t flags)
{
	uint16_t addr_reg = ADDR_REG + po;
	uint16_t data_reg = DATA_REG + po;

	uint8_t bckp = inb(addr_reg);
	outb(addr_reg, reg);
	uint8_t data = inb(data_reg);
	outb(data_reg, data & ~flags);
	outb(addr_reg, bckp);
}

void vga_init()
{
	vga_buffer = (display *)DISPLAY_ADRESS;
	po = 0x20 * (inb(MISC_OUT_READ) & ADDR_OFFSET);
}

void vga_set_entry_at(char c, display_color color, size_t row, size_t column)
{
	vga_buffer[row * VGA_WIDTH + column] = vga_entry(c, color);
}

display vga_get_entry_at(size_t row, size_t column)
{
	return vga_buffer[row * VGA_WIDTH + column];
}

void vga_show_cursor()
{
	crt_clear_flags(CURSOR_START, DISABLE_CURSOR);
}

void vga_hide_cursor()
{
	crt_set_flags(CURSOR_START, DISABLE_CURSOR);
}

void vga_set_cursor(size_t row, size_t column)
{
	size_t ind = row * VGA_WIDTH + column;
	crt_write(CURSOR_LOC_LO, ind & 0x00FF);
	crt_write(CURSOR_LOC_HI, ind >> 8);
}
