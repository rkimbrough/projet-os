#ifndef CONSTANTS_H
#define CONSTANTS_H 1

typedef enum _privilege
{
	KERNEL_PRIVILEGE = 0,
	USER_PRIVILEGE = 3
} privilege;

typedef enum _segments
{
	KERNEL_CODE_SEG	= 0x08,
	KERNEL_DATA_SEG	= 0x10,
	USER_CODE_SEG	= 0x18,
	USER_DATA_SEG	= 0x20,
	TSS_SEG			= 0x28
} segments;


#endif //CONSTANTS_H
