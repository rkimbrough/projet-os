/*
On choisit pour l'instant le modèle plat pour la segmentation (peu de segments,
et chaque segment couvre toute la mémoire). J'explique la structure des choses,
et en pratique ce que ça donne dans notre cas d'utilisation.


Structure d'un descripteur (non nul) : 2 * 4 = 8 octets, arrangés comme-ci
-----------------------------------------------------------------
|63			56	|55	52	|51	48	|47			40	|39			32	|
-----------------------------------------------------------------
|Base			|Flags	|Limit	|Access Byte	|Base			|
|31			24	|3	0	|19	16	|7			0	|23			16	|
-----------------------------------------------------------------

-----------------------------------------------------------------
|31							16	|15							0	|
-----------------------------------------------------------------
|Base							|Limit							|
|15							0	|15							0	|
-----------------------------------------------------------------

Base est l'adresse de début du segment, Limit est l'indice maximal d'un bloc
dans le segment (cf. explication des flags).

Modèle plat : Base vaudra toujours 0 et Limit toujours 0xFFFFF (chaque segment
prend toute la mémoire).


Structure de Access Byte : 1 + 2 + 5 = 8 bits, arrangés comme-ci
---------------------------------
|P	|DPL	|Type				|
---------------------------------

P est le bit de présence, utilisé pour indiquer qu'un segment est présent en
mémoire et doit valoir 1 pour tout segment valide.

DPL indique les privilèges du segment, allant de 0 (la plus forte, typiquement
le kernel) à 3 (la plus faible, typiquement l'utilisateur)

Voici la liste des types de segment possible :
	0x01	-> TSS 16-bit libre		[Protected mode seulement]
	0x02	-> LDT
	0x03	-> TSS 16-bit occupé	[Protected mode seulement]
	0x09	-> TSS libre
	0x0B	-> TSS occupé
	0x10	-> Read only, peut-être étendu vers le haut
	0x12	-> Read/Write, peut-être étendu vers le haut
	0x14	-> Read only, peut-être étendu vers le bas
	0x16	-> Read/Write, peut-être étendu vers le bas
	0x18	-> Execute only, exécutable seulement par du code au même niveau de
	privilège
	0x1A	-> Execute/Read, exécutable seulement par du code au même niveau de
	privilège
	0x1C	-> Execute only, exécutable par n'importe quel code avec un niveau
	de privilège plus faible
	0x1E	-> Execute/Read, exécutable par n'importe quel code avec un niveau
	de privilège plus faible


Structure des flags : 4 bits, arrangés comme-ci
-----------------
|G	|Size	|0	|
-----------------

G est le flag de granularité, qui définit la taille des blocs :
	0	-> blocs de 1 octet
	1	-> blocs de 4KiB (0x1000 octets)

Size indique la bit size du segment :
	0	-> 16-bit
	1	-> 64-bit, uniquement pour des segments de CODE 64-bit
	2	-> 32-bit
	3	-> impossible

Le bit en position 0 est réservé au CPU et doit toujours être mis à 0.

Modèle plat : Flags vaudra 0b1100 = 0xC. On aura G = 1 et Limit = 0xFFFFF, ce
qui veut dire que l'on utilise 2^20 blocs de taille 2^12 octets pour chaque
segment, soit 2^32 octets, c'est à dire l'entièreté de la mémoire disponible.
*/

#ifndef GDT_H
#define GDT_H 1

#include "util/constants.h"

#include <stdint.h>

//-- Constantes utiles --
//Type
typedef enum _segment_type
{
	TSS_16_FREE	= 0x01,
	LDT			= 0x02,
	TSS_16_BUSY	= 0x03,
	TSS_FREE	= 0x09,
	TSS_BUSY	= 0x0B,
	R_UP		= 0x10,
	RW_UP		= 0x12,
	R_DOWN		= 0x14,
	RW_DOWN		= 0x16,
	E_NC		= 0x18,
	ER_NC		= 0x1A,
	E_C			= 0x1C,
	ER_C		= 0x1E
} segment_type;

//Granularité
typedef enum _segment_granularity
{
	BYTE_GRANULARITY = 0,
	PAGE_GRANULARITY = 1
} segment_granularity;

//Taille
typedef enum _segment_bitwidth
{
	SIZE_16 = 0,
	SIZE_32 = 2,
	SIZE_64 = 1
} segment_bitwidth;

//-- Structures --
typedef struct _segment_descriptor
{
	uint16_t limit_0_15	: 16;
	uint16_t base_0_15	: 16;
	uint8_t base_16_23	: 8;
	uint8_t access		: 8;
	uint8_t limit_16_19	: 4;
	uint8_t flags		: 4;
	uint8_t base_24_31	: 8;
}__attribute__((packed)) segment_descriptor;
#define SEGMENT_DESCRIPTOR_SIZE 8

typedef struct _gdt_descriptor
{
	uint16_t size;
	segment_descriptor *address;
}__attribute__((packed)) gdt_descriptor;

//Fonction pour initialiser un segment_descriptor
void init_segment_descriptor(segment_descriptor *desc,
							uint32_t base,
							uint32_t limit,
							uint8_t P,
							privilege DPL,
							segment_type type,
							segment_granularity G,
							segment_bitwidth size);

#endif //GDT_H
