/*
Ce fichier :
 - implémente la fonction init_segment_descriptor déclarée dans gdt.h
 - crée le GDT minimal qui nous servira
*/

#include "gdt.h"

void init_segment_descriptor(segment_descriptor *desc,
							uint32_t base,
							uint32_t limit,
							uint8_t P,
							privilege DPL,
							segment_type type,
							segment_granularity G,
							segment_bitwidth size)
{
	desc->base_0_15		= (base & 0x0000FFFF);
	desc->base_16_23	= (base & 0x00FF0000) >> 16;
	desc->base_24_31	= (base & 0xFF000000) >> 24;
	desc->limit_0_15	= (limit & 0x0FFFF);
	desc->limit_16_19	= (limit & 0xF0000) >> 16;
	desc->access		= (P & 0b1) << 7 | (DPL & 0b11) << 5 | (type & 0b11111);
	desc->flags			= (G & 0b1) << 3 | (size & 0b11) << 1;
}

#define FLAT_BASE 0x00000000
#define FLAT_LIMIT 0xFFFFF
#define GDT_SIZE 6

static segment_descriptor gdt[GDT_SIZE];
extern gdt_descriptor gdt_desc;

void create_gdt()
{
	//Null descriptor
	init_segment_descriptor(gdt, 0, 0, 0, 0, 0, 0, 0);
	//Kernel code
	init_segment_descriptor(gdt + 1, FLAT_BASE, FLAT_LIMIT, 1, KERNEL_PRIVILEGE, ER_NC, PAGE_GRANULARITY, SIZE_32);
	//Kernel data
	init_segment_descriptor(gdt + 2, FLAT_BASE, FLAT_LIMIT, 1, KERNEL_PRIVILEGE, RW_UP, PAGE_GRANULARITY, SIZE_32);
	//User code
	init_segment_descriptor(gdt + 3, FLAT_BASE, FLAT_LIMIT, 1, USER_PRIVILEGE, ER_NC, PAGE_GRANULARITY, SIZE_32);
	//User data
	init_segment_descriptor(gdt + 4, FLAT_BASE, FLAT_LIMIT, 1, USER_PRIVILEGE, RW_UP, PAGE_GRANULARITY, SIZE_32);
	//TSS
	//TODO

	gdt_desc.address = gdt;
	gdt_desc.size = GDT_SIZE * SEGMENT_DESCRIPTOR_SIZE;
}
