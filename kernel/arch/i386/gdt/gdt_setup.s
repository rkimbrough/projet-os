.global		gdt_desc, load_gdt, load_segments
.type		gdt_desc, @object
gdt_desc:
	.word	0
	.long	0

.type		load_gdt, @function
.type		load_segments, @function
load_gdt:
	cli
	lgdt	(gdt_desc)

load_segments:
	ljmp	$KERNEL_CODE_SEG, $load_cs
load_cs:
	mov		$KERNEL_DATA_SEG, %ax
	mov		%ax, %ds
	mov		%ax, %ss
	mov		%ax, %es
	mov		%ax, %fs
	mov		%ax, %gs
	ret

