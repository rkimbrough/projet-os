#include <stdint.h>
#ifndef INIT_H
#define INIT_H 1

#include <stdint.h>

// ??
#define ENTRY_SIZE 4

typedef struct _page_common_bits {
	uint8_t present : 1;
	uint8_t read_write : 1;
	uint8_t user_superviser : 1;
	uint8_t page_write_through : 1;
	uint8_t page_cache_disable : 1;
	uint8_t accessed : 1;
}__attribute__((packed)) page_common_bits;

// review syntax coherence w/ remy's descriptors
typedef struct _pde_descriptor_4k {
	uint32_t oui: 32;
}__attribute__((packed)) pde_descriptor_4k;

typedef struct _pd_descriptor {
	pde_descriptor_4k *address;
}__attribute__((packed)) pd_decriptor_4k;

/* not sure if we do the 4M paging 
typedef struct _pd_descriptor_4M {
	uint16_t address_1 : 10;
	uint8_t rsvd : 1; // always 0 i don't know what it is doing
	uint8_t address_2 : 8; // add[39-12]
	uint8_t pat : 1; // reserved dunno if I put it in 
	uint8_t avl : 3;
	uint8_t g : 1;
	uint8_t ps : 1; // always 1 : to initialize
	uint8_t dirty : 1;
	page_common_bits page_common_bits : 6;
}__attribute__((packed)) pd_descriptor_4M;
*/

typedef struct _pte_descriptor {
	uint32_t oui: 32;
}__attribute__((packed)) pte_descriptor;

typedef struct _pt_descriptor {
	pte_descriptor *address;
}__attribute__((packed)) pt_decriptor;

typedef struct _virtual_address_bits {
	uint16_t offset : 12;
	uint16_t idx_pt : 10;
	uint16_t idx_pd : 10;
}__attribute__((packed)) virtual_address_bits;

// function to populate the pd
void empty_pd_descr(pde_descriptor_4k *pd);
void empty_pt_descr(pte_descriptor *pt);

#endif //INIT_H
