#include <kernel/page_allocation.h>
#include <stdint.h>

buddy buddy_alloc;

void init_alloc_system(buddy *buddy_alloc) {
	for (int i = 0; i<BUDDY_0_SIZE; i++) {
		(buddy_alloc->buddy_0)[i] = 0;
		(buddy_alloc->buddy_1)[i>>1] = 0;
		(buddy_alloc->buddy_2)[i>>2] = 0;
		(buddy_alloc->buddy_3)[i>>3] = 0;
		(buddy_alloc->buddy_4)[i>>4] = 0;
		(buddy_alloc->buddy_5)[i>>5] = 0;
		(buddy_alloc->buddy_6)[i>>6] = 0;
		(buddy_alloc->buddy_7)[i>>7] = 0;
	}

	for (int i = 0; i<1024; i++) {
		mark_page_as_used(i);
		mark_page_as_used(768*1024 + i);
	}
}

void access_buddy(int n_buddy, int index, int offset, buddy *buddy_alloc) {
	switch (n_buddy) {
		case 0:
			(buddy_alloc->buddy_0)[index] |= 1<<offset;
			break;
		case 1:
			(buddy_alloc->buddy_1)[index] |= 1<<offset;
			break;
		case 2:
			(buddy_alloc->buddy_2)[index] |= 1<<offset;
			break;
		case 3:
			(buddy_alloc->buddy_3)[index] |= 1<<offset;
			break;
		case 4:
			(buddy_alloc->buddy_4)[index] |= 1<<offset;
			break;
		case 5:
			(buddy_alloc->buddy_5)[index] |= 1<<offset;
			break;
		case 6:
			(buddy_alloc->buddy_6)[index] |= 1<<offset;
			break;
		case 7:
			(buddy_alloc->buddy_7)[index] |= 1<<offset;
			break;
	}
}

void mark_page_as_used(uint32_t page_number) {
	for (int i = 0; i<=7; i++) {
		uint32_t block_number = page_number >> i;	
		int idx = block_number / 32;
		int off = block_number % 32;
		access_buddy(i, idx, off, &buddy_alloc);
	}
}

void on_page_fault(uint32_t, uint32_t) {
	// todo -> allocation un peu nulle
}
