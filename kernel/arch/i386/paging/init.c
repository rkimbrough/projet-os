#include "init.h"
#include <kernel/tty.h>
#include <stdint.h>

// TODO : the tables : 2 of them PD -> PT -> offset to the physical address
// TODO : identity mapping and higher half kernel
// 4 GiB memory : put kernel ALL THE WAY UP
// First MiB : identity paging : all the booting stuff to not break all the stuff already set up

#define TABLE_SIZE 1024

// populate the tables 
// when PDE.P = 0 every bit is ignored by the processor
void empty_pd_descr(pde_descriptor_4k *pd) {
	pd->oui = 0;
}

void init_pt(pte_descriptor *pt, uint32_t add) {
	pt->oui = 1 | (0xFFFFF000 & add);
}

// tables for translation 
static pde_descriptor_4k pd[TABLE_SIZE] __attribute__((aligned(4096)));
static pte_descriptor first_pt[TABLE_SIZE] __attribute__((aligned(4096)));
static pte_descriptor kernel_pt[TABLE_SIZE] __attribute__((aligned(4096)));

extern pd_decriptor_4k pd_desc;

void init_tables() {
	for (int i=0; i<1024; i++) {
		empty_pd_descr(pd + i);
		init_pt(first_pt + i, (i*0x1000));
		init_pt(kernel_pt + i, (i*0x1000));
 }
	// identity mapping the first MB
	// 0x00000000 -> 0x000fffff
	pd[0].oui |= 1;
	pd[0].oui |= 0xFFFFF000 & (uint32_t)first_pt;
	// higher half kernel (0xC0000000 >> 12 = 768)
	pd[768].oui |= 1;
	pd[768].oui |= 0xFFFFF000 & (uint32_t)first_pt;
	pd_desc.address = pd;
	term_writestring("Pages initialized.\n");//while (1) {}
};
