.global   pd_desc, enb_paging
.type		pd_desc, @object

pd_desc:
	.long	0

.type enb_paging, @function

# enable paging
enb_paging:
 mov $pd_desc, %ebx
 mov (%ebx), %eax
 mov %eax, %cr3
 
 mov %cr0, %eax
 or $0x80000000, %eax
 mov %eax, %cr0
 # not returning properly ? why ?
 ret
