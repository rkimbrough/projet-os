/*
On dispose de deux PICs (Programmable Interrupt Controller), un PIC maître et
un PIC esclave. Chaque PIC possède 8 canaux pour recevoir des interrupt, et il
est conventionnel que le maître communique avec l'esclave par son canal 2.

Chaque PIC communique avec nous via deux octets à une adresse fixe, le premier
servant de canal de commandes et le deuxième de canal de données.

La communication peut avec le PIC peut se faire de 7 manières différentes :
 - 4 Initialisation Command Word (ICW1 - ICW4)
 - 3 Operation Command Word (OCW1 - OCW3)
Pour ICW1, OCW2 et OCW3, il faut envoyer un octet via le canal de commandes.
Pour ICW2, ICW3, ICW4 et OCW1, il faut envoyer un octet via le canal de
données.

Il y a deux modes possible : 8080 et 8086. Nous sommes en 8086, je décrirai
donc la structure des différents éléments pour ce mode-ci.


Structure des CW :
	ICW1 : 3 + 1 + 1 + 1 + 1 + 1 = 8 bits
	---------------------------------
	|0			|1	|LT	|0	|S	|I4	|
	---------------------------------
	LT indique si l'on est en level-triggered ou edge-triggered mode :
		0 -> edge-triggered, quelque chose (?) ne se passe qu'aux instants où
		un évènement se déclenche
		1 -> level-triggered, quelque chose (?) se passe tant qu'un évènement
		est en cours
	On n'a à priori pas de raison de ne pas être en edge-triggered.

	S spécifie si le PIC fonctionne seul ou en cascade dans une paire
	maître-esclave :
		0 -> en cascade
		1 -> seul
	Dans notre cas, on possède deux PICs donc S = 0.
	C'est ce bit qui précise à l'initialisation si lon attend ICW3 ou non.

	I4 précise si l'on attend ICW4 ou non à l'initialisation :
		0 -> pas de ICW4
		1 -> ICW4 attendu
	

	ICW2 : 5 + 3 = 0 bits
	---------------------------------
	|T_3_7				|0			|
	---------------------------------
	T_3_7 est le décalage des interrupt du PIC dans l'IDT. Si le PIC lance
	un IRQ n, l'adresse accédée dans l'IDT est (T_3_7 << 3) | n.


	ICW3 pour le PIC maître : 8 bits, le i-ème vaut 1 si un esclave est
	rattaché au canal i.

	ICW3 pour le PIC esclave : 5 + 3 = 8 bits
	---------------------------------
	|0					|ID			|
	---------------------------------
	ID est l'identifiant d'esclave du PIC.


	ICW4 : 4 + 2 + 1 + 1 = 8 bits
	---------------------------------
	|0				|B		|A	|1	|
	---------------------------------
	B spécifie si le PIC fonctionne avec un buffer ou non. On peut en plus
	spécifier si le PIC fonctionne alors comme maître ou comme esclave :
		0 -> pas de buffer
		1 -> pas de buffer
		2 -> PIC esclave, avec un buffer
		3 -> PIC maître, avec un buffer

	A spécifie si l'on est en Automatic EOI (End Of Interrupt) Mode ou non :
		0 -> EOI normaux
		1 -> EOI automatiques
	

	OCW1 : 8 bits, le i-ème d'entre eux vaut 1 si le canal i est masqué.


	OCW2 : TODO. Pour l'instant, on s'intéresse seulement à EOI (End Of
	Interrupt), 0b00100000.


	OCW3 : TODO. Pour l'instant, on s'intéresse seulement à la lecture des
	registres IRR (0b00001010) et ISR (0b00001011), cf. le paragraphe suivant.


En outre, le PIC possède deux registres : In-Service Register et Interrupt
Request Register. Le premier nous dit quels interrupt ont été envoyés au CPU,
et le deuxième indique quels interrupt sont actuellement en attente.
Il est bon de pouvoir y accéder pour gérer le cas où un interrupt a disparu
entre le moment où le PIC a alerté le controlleur et le moment où le PIC
envoie le numéro de l'interrupt (il envoie alors toujours 7 ou 15).

Cf https://wiki.osdev.org/PIC#Spurious_IRQs pour plus d'explications.
*/

#include "pic.h"

#include <kernel/portio.h>
#include <kernel/interrupt.h>
#include <kernel/tty.h>

#include <stdbool.h>

#define MASTER_COMMAND	0x20
#define MASTER_DATA		0x21
#define SLAVE_COMMAND	0xA0
#define SLAVE_DATA		0xA1

#define ICW1			0b00010001
#define ICW4			0b00000001
#define SLAVE_ID		2

typedef enum _ocw2
{
	EOI = 0b00100000
} ocw2;

typedef enum _ocw3
{
	READ_IRR = 0b00001010,
	READ_ISR = 0b00001011
} ocw3;

void send_EOI(unsigned char irq)
{
	if (irq >= 8)
		outb(SLAVE_COMMAND, EOI);
	outb(MASTER_COMMAND, EOI);
}

bool handle_spurious_7()
{
	return (~PIC_get_isr() >> 7 & 1);
}

bool handle_spurious_15()
{
	if (~PIC_get_isr() >> 15 & 1)
	{
		send_EOI(SLAVE_ID);
		return true;
	}
	return false;
}

uint16_t PIC_get_reg(ocw3 reg)
{
	outb(MASTER_COMMAND, reg);
	outb(SLAVE_COMMAND, reg);
	return inb(SLAVE_COMMAND) << 8 | inb(MASTER_COMMAND);
}

uint16_t PIC_get_irr()
{
	return PIC_get_reg(READ_IRR);
}

uint16_t PIC_get_isr()
{
	return PIC_get_reg(READ_ISR);
}

void PIC_set_mask(unsigned char chan)
{
	uint16_t port;
	uint8_t value;

	if (chan < 8)
		port = MASTER_DATA;
	else
	{
		chan -= 8;
		port = SLAVE_DATA;
	}

	value = inb(port) | (1 << chan);
	outb(port, value);
}

void disable_irq(irq_number irq)
{
	PIC_set_mask(irq);
}

void PIC_clear_mask(unsigned char chan)
{
	uint16_t port;
	uint8_t value;

	if (chan < 8)
		port = MASTER_DATA;
	else
	{
		chan -= 8;
		port = SLAVE_DATA;
	}

	value = inb(port) & ~(1 << chan);
	outb(port, value);
}

void enable_irq(irq_number irq)
{
	PIC_clear_mask(irq);
}

void PIC_set_all()
{
	outb(SLAVE_DATA, 0xFF);
	outb(MASTER_DATA, 0xFF);
}

void PIC_clear_all()
{
	outb(MASTER_DATA, 0);
	outb(SLAVE_DATA, 0);
}

void PIC_remap(uint8_t master_offset, uint8_t slave_offset)
{
	//On sauvegarde les masques
	unsigned char master_mask, slave_mask;
	master_mask = inb(MASTER_DATA);
	slave_mask = inb(SLAVE_DATA);

	//On envoie ICW1 pour réinitialiser
	outb(MASTER_COMMAND, ICW1);
	io_wait();
	outb(SLAVE_COMMAND, ICW1);
	io_wait();

	//On précise les offset (ICW2), alignés modulo 8
	outb(MASTER_DATA, master_offset);
	io_wait();
	outb(SLAVE_DATA, slave_offset);
	io_wait();

	//On chaîne l'esclave au maître (ICW3), sur le canal 2
	outb(MASTER_DATA, 1 << SLAVE_ID);
	io_wait();
	outb(SLAVE_DATA, SLAVE_ID);
	io_wait();

	//On ne veut pas fonctionner dans un mode particulier
	outb(MASTER_DATA, ICW4);
	io_wait();
	outb(SLAVE_DATA, ICW4);
	io_wait();

	//On restitue les masques sauvegardés
	outb(MASTER_DATA, master_mask);
	outb(SLAVE_DATA, slave_mask);
}

void PIC_init()
{
	PIC_set_all();
	PIC_remap(IRQ_START, IRQ_START + 8);
	PIC_clear_mask(SLAVE_ID);
	term_writestring("PIC initialized.\n");
}