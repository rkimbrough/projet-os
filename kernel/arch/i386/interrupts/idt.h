/*
Structure d'un descripteur (non nul) : 2 * 4 = 8 octets, arrangés comme-ci
-----------------------------------------------------------------
|63			56	|55	52	|51	48	|47			40	|39			32	|
-----------------------------------------------------------------
|Offset							|Info Byte		|0				|
|31							16	|7			0	|				|
-----------------------------------------------------------------

-----------------------------------------------------------------
|31							16	|15							0	|
-----------------------------------------------------------------
|Segment Selector				|Offset							|
|15							0	|15							0	|
-----------------------------------------------------------------

Offset est l'adresse de l'interrupt handler, Segment Selector est le selecteur
du segment dans lequel se trouve cet IH.


Structure de Info Byte : 1 + 2 + 1 + 4 = 8 bits, arrangés comme-ci
---------------------------------
|P	|DPL	|0	|Type			|
---------------------------------

P est le bit de présence et doit valoir 1 pour tout descripteur valide.

DPL indique les privilèges du segment, allant de 0 (la plus forte, typiquement
le kernel) à 3 (la plus faible, typiquement l'utilisateur)

Voici la liste des types de porte possible :
	0x5	-> Task Gate				[Offset est alors inutilisé et doit être mis à 0]
	0x6	-> Interrupt Gate 16-bit	[Protected mode seulement]
	0x7	-> Trap Gate 16-bit			[Protected mode seulement]
	0xE	-> Interrupt Gate
	0xF	-> Trap Gate
*/

#ifndef IDT_H
#define IDT_H 1

#include "util/constants.h"

#include <stdint.h>

//-- Constantes utiles --
//Types
typedef enum _gate_type
{
	TASK 			= 0x5,
	INTERRUPT_16	= 0x6,
	TRAP_16			= 0x7,
	INTERRUPT		= 0xE,
	TRAP			= 0xF
} gate_type;

//Offset = 0 pour une Task Gate
#define TASK_OFFSET		0

//-- Structures --
typedef struct _gate_descriptor
{
	uint16_t offset_0_15	: 16;
	uint16_t selector		: 16;
	uint8_t zero			: 8;
	uint8_t info			: 8;
	uint16_t offset_16_31	: 16;
}__attribute__((packed)) gate_descriptor;
#define GATE_DESCRIPTOR_SIZE 8

typedef struct _idt_descriptor
{
	uint16_t size;
	gate_descriptor *address;
}__attribute__((packed)) idt_descriptor;

//Fonction pour initialiser un gate_descriptor
void init_gate_descriptor(gate_descriptor *desc,
							uint32_t offset,
							uint16_t selector,
							uint8_t P,
							privilege DPL,
							gate_type type);

#endif //IDT_H
