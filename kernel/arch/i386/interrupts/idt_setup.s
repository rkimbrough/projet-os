.global		idt_desc, load_idt
.type		idt_desc, @object
idt_desc:
	.word	0
	.long	0

.type		load_idt, @function
load_idt:
	cli
	lidt	(idt_desc)
	ret
