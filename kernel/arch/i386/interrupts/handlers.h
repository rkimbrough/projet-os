#ifndef HANDLERS_H
#define HANDLERS_H 1

#include <kernel/interrupt.h>

#include <stdint.h>

struct _interrupt_frame
{
	uint32_t instruction_pointer;
	uint32_t code_segment;
	uint32_t flags;
	uint32_t stack_pointer;
	uint32_t stack_segment;
}__attribute__((packed));

void interrupt_handler_noerr(interrupt_frame *frame);
void interrupt_handler_err(interrupt_frame *frame, unsigned int err);
void page_fault_handler(interrupt_frame *frame, unsigned int err);

#define NOERR(n) void int_ ## n(interrupt_frame *frame)
#define ERR(n) void int_ ## n(interrupt_frame *frame, unsigned int err)

NOERR(0);
NOERR(1);
NOERR(2);
NOERR(3);
NOERR(4);
NOERR(5);
NOERR(6);
NOERR(7);
ERR(8);
NOERR(9);
ERR(10);
ERR(11);
ERR(12);
ERR(13);
//ERR(14);
NOERR(16);
ERR(17);
NOERR(18);
NOERR(19);
NOERR(20);
ERR(21);

#endif //HANDLERS_H
