#include "idt.h"
#include "handlers.h"

#include <kernel/interrupt.h>

void init_gate_descriptor(gate_descriptor *desc,
							uint32_t offset,
							uint16_t selector,
							uint8_t P,
							privilege DPL,
							gate_type type)
{
	desc->offset_0_15	= (0x0000FFFF & offset);
	desc->offset_16_31	= (0xFFFF0000 & offset) >> 16;
	desc->selector		= selector;
	desc->zero			= 0;
	desc->info			= (P & 0b1) << 7 | (DPL & 0b11) << 5 | (type & 0b1111);
}

#define IDT_SIZE 256

static gate_descriptor idt[IDT_SIZE];
extern idt_descriptor idt_desc;

void set_interrupt(int n_int, void (*isr)(interrupt_frame *))
{
	init_gate_descriptor(idt + n_int, (int32_t)isr, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, INTERRUPT);
}

void create_idt()
{
	init_gate_descriptor(idt, (int32_t)int_0, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 1, (int32_t)int_1, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 2, (int32_t)int_2, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, INTERRUPT);
	init_gate_descriptor(idt + 3, (int32_t)int_3, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 4, (int32_t)int_4, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 5, (int32_t)int_5, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 6, (int32_t)int_6, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 7, (int32_t)int_7, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 8, (int32_t)int_8, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 9, (int32_t)int_9, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 10, (int32_t)int_10, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 11, (int32_t)int_11, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 12, (int32_t)int_12, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 13, (int32_t)int_13, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 14, (int32_t)page_fault_handler, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 16, (int32_t)int_16, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 17, (int32_t)int_17, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 18, (int32_t)int_18, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 19, (int32_t)int_19, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 20, (int32_t)int_20, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);
	init_gate_descriptor(idt + 21, (int32_t)int_21, KERNEL_CODE_SEG, 1, KERNEL_PRIVILEGE, TRAP);

	idt_desc.size = IDT_SIZE * GATE_DESCRIPTOR_SIZE;
	idt_desc.address = idt;
}