#include "handlers.h"

#include <kernel/tty.h>


__attribute__((interrupt))
void interrupt_handler_noerr(interrupt_frame *frame __attribute__((unused)))
{
	term_writestring("interrupt sans code d'erreur lolilol\n");
	asm volatile ("cli; hlt");
}

__attribute__((interrupt))
void interrupt_handler_err(interrupt_frame *frame __attribute__((unused)), unsigned int err __attribute__((unused)))
{
	term_writestring("thomazo\n");
	asm volatile ("cli; hlt");
}

#define DNOERR(n) \
__attribute__((interrupt)) \
void int_ ## n(interrupt_frame *frame __attribute__((unused))) \
{ \
	term_putchar(65 + n); \
	asm volatile ("cli; hlt"); \
}
#define DERR(n) \
__attribute__((interrupt)) \
void int_ ## n(interrupt_frame *frame __attribute__((unused)), unsigned int err __attribute__((unused))) \
{ \
	term_putchar(65 + n); /*ajouter l'affichage de instruction pointer*/ \
	asm volatile ("cli; hlt"); \
}

__attribute__((interrupt))
void page_fault_handler(interrupt_frame *frame __attribute__((unused)), unsigned int err )
{
	if (err & 1) {
		term_writestring("accès pas autorisé");
	} else {
		term_writestring("Problème d'existence\n");
	}

	uint32_t add;
	asm volatile ("mov %%cr2, %0" : "=a"(add));
	term_writestring("cr2 loaded\n");

	// display cr2 value
	for (uint8_t i = 32; i>0; i--) {
		uint32_t j = 1 << i;
		if (add & j) {
			term_putchar('1');
		} else {
			term_putchar('0');
		}
	}
	term_putchar('\n');

	asm volatile ("cli; hlt");
}

DNOERR(0);
DNOERR(1);
DNOERR(2);
DNOERR(3);
DNOERR(4);
DNOERR(5);
DNOERR(6);
DNOERR(7);
DERR(8);
DNOERR(9);
DERR(10);
DERR(11);
DERR(12);
DERR(13);
//DERR(14);
DNOERR(16);
DERR(17);
DNOERR(18);
DNOERR(19);
DNOERR(20);
DERR(21);
