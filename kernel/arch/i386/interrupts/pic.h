#ifndef PIC_H
#define PIC_H 1

#include <stdint.h>

uint16_t PIC_get_irr();
uint16_t PIC_get_isr();
void PIC_set_mask(unsigned char chan);
void PIC_clear_mask(unsigned char chan);
void PIC_set_all();
void PIC_clear_all();

#endif //PIC_H
