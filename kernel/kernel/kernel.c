#include "hardware.h"
#include "ui.h"
#include "modes.h"

#include <kernel/msg.h>

static int selector = 0;

void sleep(uint64_t ticks)
{
	uint64_t goal = real_ticks + ticks;
	while (real_ticks < goal) {}
}

static inline void show_option(int sel, const char *name)
{
	if (selector == sel)
		term_color_swap();
	term_set_cursor(MODE_DESC + 1 + (sel / 4), 20 * (sel % 4));
	term_writestring(name);
	if (selector == sel)
		term_color_swap();
}

void selection_init()
{
	term_clear();
	print_ascii_pianos();
	set_title("Mode Selection");
	term_set_cursor(MODE_DESC, 0);
	term_writestring("Select your mode:");
	selector = 0;
}

void kernel_main()
{
	hardware_init();
	reset_msgq();
	close_msgq();

	//On montre qu'on sait attendre
	term_writestring("\nWaking up");
	for (int i = 0; i < 3; i++)
	{
		sleep(REAL_TICK_RATE / 6);
		term_putchar('.');
	}
	sleep(REAL_TICK_RATE / 6);

	//Il faut faire une petite interface mignonne
	selection_init();
	player_reset();

	open_msgq();
	while (1)
	{
		hardware_update();
		print_time();
		player_update();

		show_option(0, "Piano Mode");
		show_option(1, "Text Mode");
		show_option(2, "Music Player");

		if (is_msg())
		{
			key_info k = ps2_kbd_driver.code_to_key(ps2_kbd, front_msg());
			pop_msg();
			if (!k.release)
			{
				switch (k.chr)
				{
				case RIGHT:
					selector = (selector + 1) % 3;
					break;
				case LEFT:
					selector = (selector + 2) % 3;
					break;
				case '\n':
					switch (selector)
					{
					case 0:
						piano();
						break;
					case 1:
						text();
						break;
					case 2:
						player();
						break;
					}

					selection_init();
					break;
				}
			}
		}
	}
}