#include "hardware.h"

keyboard *ps2_kbd;
audio_channel *pcspk;

void hardware_init()
{
	term_init();
	pit_driver.base.driver_init();
	rtc_driver.base.driver_init();
	ps2_kbd_driver.base.driver_init();
	pcspk_driver.base.driver_init();
	dma_irq_flpy_driver.base.driver_init();

	ps2_kbd = ps2_kbd_driver.add_keyboard("");
	pcspk = pcspk_driver.add_channel("");
}

void hardware_update()
{
	//Pas besoin de mettre à jour les timers ni le son
	ps2_kbd_driver.base.driver_update();
}
