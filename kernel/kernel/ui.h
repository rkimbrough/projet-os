#ifndef UI_H
#define UI_H 1

#include <kernel/vga.h>

#define ASCII_WIDTH 33
#define ASCII_HEIGHT 6
#define MODE_DESC (ASCII_HEIGHT + 2)

void print_ascii_pianos_color(vga_text_color color);
void print_ascii_pianos();
void set_title(const char *title);
void print_time();

#endif //UI_H
