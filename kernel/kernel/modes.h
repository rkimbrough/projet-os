#ifndef MODES_H
#define MODES_H 1

#include <stdbool.h>

void piano();
void text();

void player();
void player_reset();
void player_update();
bool player_pause();
bool player_resume();

#endif //MODES_H
