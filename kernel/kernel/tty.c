#include "ui.h"
#include "hardware.h"
#include "modes.h"

#include <kernel/tty.h>
#include <kernel/vga.h>
#include <kernel/msg.h>

#include <string.h>

//-- Fonctions du terminal --
static size_t term_row;
static size_t term_column;
static display_color term_color;
static bool visible_cursor;

static size_t split_line;

//déplacer le curseur
void term_show_cursor()
{
	visible_cursor = true;
	vga_show_cursor();
}

void term_hide_cursor()
{
	visible_cursor = false;
	vga_hide_cursor();
}

void term_set_cursor(size_t row, size_t col)
{
	term_row = row;
	term_column = col;
	if (visible_cursor)
		vga_set_cursor(row, col);
}

//changer de couleur
void term_set_color(vga_text_color fg, vga_text_color bg) 
{
	term_color = vga_entry_color(fg, bg);
}

display_color term_get_color()
{
	return term_color;
}

void term_color_swap()
{
	term_color = vga_entry_color(get_bg_color(term_color), get_fg_color(term_color));
}

//effacer le terminal
static inline void term_clearat(size_t row, size_t column)
{
	vga_set_entry_at(' ', term_color, row, column);
}

void term_clear_row(size_t row)
{
	for (size_t column = 0; column < VGA_WIDTH; column++)
		term_clearat(row, column);
}

void term_clear()
{
	for (size_t row = 0; row < VGA_HEIGHT; row++)
		term_clear_row(row);
}

//Changement de lignes
void term_newline(bool split)
{
	if (term_row + 1 < VGA_HEIGHT)
	{
		term_set_cursor(term_row + 1, 0);
		split_line = (split ? split_line + 1 : 0);
	}
}

//initialisation
void term_init()
{
	vga_init();
	term_hide_cursor();
	term_set_cursor(0, 0);
	term_set_color(VGA_WHITE, VGA_BLACK);
	split_line = 0;
	term_clear();
	term_writestring("Terminal initialized.\n");
}

//affichage
void term_putchar(char c)
{
	switch (c)
	{
	case '\n':
		term_newline(false);
		break;
	case '\b':
		if (term_column > 0)
			term_set_cursor(term_row, term_column - 1);
		else if (split_line > 0)
		{
			term_set_cursor(term_row - 1, VGA_WIDTH - 1);
			split_line--;
		}
		vga_set_entry_at(' ', term_color, term_row, term_column);
		break;
	default:
		vga_set_entry_at(c, term_color, term_row, term_column);
		if (term_column < VGA_WIDTH)
			term_set_cursor(term_row, term_column + 1);
		if (term_column == VGA_WIDTH)
			term_newline(true);
	}
}

void term_write(const char *data, size_t size)
{
	for (size_t i = 0; i < size; i++)
		term_putchar(data[i]);
}

void term_writestring(const char *str)
{
	term_write(str, strlen(str));
}

//-- Text mode --
void text()
{
	set_title("Text Mode");
	term_clear_row(MODE_DESC);
	term_clear_row(MODE_DESC + 1);

	term_set_cursor(MODE_DESC, 0);
	term_writestring("Quit: ESC");

	term_show_cursor();
	term_set_cursor(MODE_DESC + 2, 0);
	while (1)
	{
		hardware_update();

		size_t row_bckp = term_row;
		size_t col_bckp = term_column;
		visible_cursor = false;
		print_time();
		visible_cursor = true;
		term_row = row_bckp;
		term_column = col_bckp;

		player_update();

		if (is_msg())
		{
			key_info k = ps2_kbd_driver.code_to_key(ps2_kbd, front_msg());
			pop_msg();

			if (k.chr == '\e')
				break;
			if (!k.release && k.chr != '\0')
				term_putchar(k.chr);
		}
	}

	term_hide_cursor();
}
