#include <kernel/msg.h>

#define MSGQ_MAX_SIZE 20

sc_t msgq[MSGQ_MAX_SIZE];
static volatile unsigned int msgq_front;
static volatile unsigned int msgq_back;
static volatile unsigned int msgq_size;
static volatile bool discard;

void close_msgq()
{
	discard = true;
}

void open_msgq()
{
	discard = false;
}

void reset_msgq()
{
	msgq_front = 0;
	msgq_back = 0;
	msgq_size = 0;
	discard = false;
}

void push_msg(sc_t msg)
{
	if (discard)
		return;
	if (msgq_size == MSGQ_MAX_SIZE)
		{} //panic

	msgq[msgq_front] = msg;
	msgq_front = (msgq_front + 1) % MSGQ_MAX_SIZE;
	msgq_size++;
}

sc_t front_msg()
{
	if (msgq_size == 0)
		{} //panic

	sc_t msg = msgq[msgq_back];
	return msg;
}

void pop_msg()
{
	if (msgq_size == 0)
		{} //panic
	msgq_back = (msgq_back + 1) % MSGQ_MAX_SIZE;
	msgq_size--;
}

bool is_msg()
{
	return (msgq_size != 0);
}
