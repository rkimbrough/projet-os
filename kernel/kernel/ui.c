#include "ui.h"

#include <kernel/timer.h>
#include <kernel/tty.h>

#include <string.h>

static const char *ascii_pianos[ASCII_HEIGHT] = {
	"______ _             _____ _____",
	"| ___ (_)           |  _  /  ___|",
	"| |_/ /_  __ _ _ __ | | | \\ `--.",
	"|  __/| |/ _` | '_ \\| | | |`--. \\",
	"| |   | | (_| | | | \\ \\_/ /\\__/ /",
	"\\_|   |_|\\__,_|_| |_|\\___/\\____/"
};

void print_ascii_pianos_color(vga_text_color color)
{
	display_color bckp = term_get_color();
	term_set_color(color, get_bg_color(bckp));

	for (int i = 0; i < ASCII_HEIGHT; i++)
	{
		term_set_cursor(i + 1, (VGA_WIDTH - ASCII_WIDTH) / 2);
		term_writestring(ascii_pianos[i]);
	}

	term_set_color(get_fg_color(bckp), get_bg_color(bckp));
}

void print_ascii_pianos()
{
	for (int i = 0; i < ASCII_HEIGHT; i++)
	{
		term_set_cursor(i + 1, (VGA_WIDTH - ASCII_WIDTH) / 2);
		term_writestring(ascii_pianos[i]);
	}
}

void set_title(const char *title)
{
	term_color_swap();
	term_clear_row(0);
	term_set_cursor(0, (VGA_WIDTH - strlen(title)) / 2);
	term_writestring(title);
	term_color_swap();
}

inline void print_2dig(int i)
{
	term_putchar('0' + i / 10);
	term_putchar('0' + i % 10);
}

void print_time()
{
	term_color_swap();
	term_set_cursor(0, VGA_WIDTH - 10);
	print_2dig(date[DATE_DAY]);
	term_putchar('/');
	print_2dig(date[DATE_MONTH]);
	term_putchar('/');
	print_2dig(date[DATE_YEAR]);

	term_set_cursor(0, 2);
	print_2dig(date[DATE_HOURS]);
	term_putchar(':');
	print_2dig(date[DATE_MINUTES]);
	term_putchar(':');
	print_2dig(date[DATE_SECONDS]);
	term_color_swap();
}
