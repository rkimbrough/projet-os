#include "modes.h"
#include "hardware.h"
#include "ui.h"
#include "music.h"

#include <kernel/msg.h>

static size_t selector;
static size_t playing;
static size_t pos;
static uint64_t next_event;
static bool paused;

#define TRACKS 3
static const char *track[TRACKS] = {
	"E5.1.2 E5.1.2 R.1.2 E5.1.2 R.1.2 C5.1.2 E5.1.1 G5.1.1 R.1.1 G4.1.1 R.1.1 C5.1.1 R.1.2 G4.1.1 R.1.2 E4.1.1 R.1.2 A4.1.1 B4.1.1 bB4.1.2 A4.1.1 G4.2.3 E5.2.3 G5.2.3 A5.1.1 F5.1.2 G5.1.2 R.1.2 E5.1.1 C5.1.2 D5.1.2 B4.1.1 R.1.2 G5.1.2 #F5.1.2 F5.1.2 #D5.1.1 E5.1.2 R.1.2 #G4.1.2 A4.1.2 C5.1.2 R.1.2 A4.1.2 C5.1.2 D5.1.2 R.1.1 G5.1.2 #F5.1.2 F5.1.2 #D5.1.1 E5.1.2 R.1.2 C6.1.1 C6.1.2 C6.1.1 R.2.1 G5.1.2 #F5.1.2 F5.1.2 #D5.1.1 E5.1.2 R.1.2 #G4.1.2 A4.1.2 C5.1.2 R.1.2 A4.1.2 C5.1.2 D5.1.2 R.1.1 bE5.1.1 R.1.2 D5.1.1 R.1.2 C5.1.1",
	"A4.1.1 E4.3.2 A4.1.2 A4.1.4 B4.1.4 #C5.1.4 D5.1.4 E5.2.1 R.1.2 E5.1.2 E5.1.3 F5.1.3 G5.1.3 A5.2.1 R.1.2 A5.1.2 A5.1.3 G5.1.3 F5.1.3 G5.3.4 F5.1.4 E5.2.1 E5.1.1 D5.1.2 D5.1.4 E5.1.4 F5.2.1 E5.1.2 D5.1.2 C5.1.2 C5.1.4 D5.1.4 E5.2.1 D5.1.2 C5.1.2 B4.1.2 B4.1.4 #C5.1.4 #D5.2.1 #F5.1.1 E5.1.2",
	"F4.1.1 D4.1.1 F4.1.1 C4.1.1 F4.1.1 E4.1.1 B4.1.1 F4.1.1 B4.1.1 B4.1.1"
};

static const unsigned int track_bpm[TRACKS] = {
	180,
	130,
	150
};

static const char *track_name[TRACKS] = {
	"Super Mario Bros. - Main Theme",
	"The Legend of Zelda - Main Theme",
	"gitar - initialise"
};;

static inline void play(note n, note_length l, unsigned int bpm)
{
	uint64_t beat_ticks = REAL_TICK_RATE * 60 / bpm;
	next_event = real_ticks + beat_ticks * l.whole / l.fraction;

	if (n == REST)
		pcspk_driver.stop_sound(pcspk);
	else
		pcspk_driver.play_sound(pcspk, eq_temperament[n]);
}

static inline int next_is_digit()
{
	return '0' <= track[playing][pos] && track[playing][pos] <= '9';
}

static inline unsigned int read_uint_in_track()
{
	int val = 0;
	while (next_is_digit())
		val = val * 10 + (track[playing][pos++] - '0');
	return val;
}

inline void player_display()
{
	term_clear_row(MODE_DESC);
	term_clear_row(MODE_DESC + 1);
	term_set_cursor(MODE_DESC, 0);
	term_writestring("Quit: ESC");
}

void player_reset()
{
	playing = TRACKS;
	pos = 0;
	next_event = 0;
	paused = false;
}

bool player_pause()
{
	if (playing < TRACKS && !paused)
	{
		paused = true;
		return true;
	}
	return false;
}

bool player_resume()
{
	if (playing < TRACKS && paused)
	{
		paused = false;
		return true;
	}
	return false;
}

void player_stop()
{
	player_reset();
	player_display();
}

void player_update()
{
	if (playing >= TRACKS || paused || real_ticks < next_event)
		return;

	pcspk_driver.stop_sound(pcspk);
	//On ignore les espaces
	while (track[playing][pos] == ' ' || track[playing][pos] == '\t' || track[playing][pos] == '\n')
		pos++;

	note to_play;
	int alter = 0;

parse_note:
	switch (track[playing][pos])
	{
	case 'R':
		if (alter != 0)
		{
			player_stop();
			return;
		}
		to_play = REST;
		break;

	case 'b':
		if (alter > 0)
		{
			player_stop();
			return;
		}
		alter -= 1;
		pos++;
		goto parse_note;
	case '#':
		if (alter < 0)
		{
			player_stop();
			return;
		}
		alter += 1;
		pos++;
		goto parse_note;

	case 'A':
		to_play = A2 - 2 * EQ_SUBDIV;
		break;
	case 'B':
		to_play = B2 - 2 * EQ_SUBDIV;
		break;
	case 'C':
		to_play = C3 - 3 * EQ_SUBDIV;
		break;
	case 'D':
		to_play = D3 - 3 * EQ_SUBDIV;
		break;
	case 'E':
		to_play = E3 - 3 * EQ_SUBDIV;
		break;
	case 'F':
		to_play = F3 - 3 * EQ_SUBDIV;
		break;
	case 'G':
		to_play = G2 - 2 * EQ_SUBDIV;
		break;

	default:
		player_stop();
		return;
	}

	pos++;
	if (to_play != REST)
	{
		if (!next_is_digit())
		{
			player_stop();
			return;
		}

		to_play += alter + read_uint_in_track() * EQ_SUBDIV;
		if (to_play < 0 || REST < to_play)
			to_play = REST;
	}

	if (track[playing][pos] != '.')
	{
		player_stop();
		return;
	}
	pos++;

	note_length length;
	if (!next_is_digit())
	{
		player_stop();
		return;
	}
	length.whole = read_uint_in_track();

	if (track[playing][pos] != '.')
	{
		player_stop();
		return;
	}
	pos++;

	if (!next_is_digit())
	{
		player_stop();
		return;
	}
	length.fraction = read_uint_in_track();

	play(to_play, length, track_bpm[playing]);
}

static inline void show_track(size_t iTrack)
{
	term_clear_row(MODE_DESC + 2 + iTrack);
	term_set_cursor(MODE_DESC + 2 + iTrack, 0);
	term_writestring(track_name[iTrack]);
}

static inline void show_track_select(size_t iTrack)
{
	term_color_swap();
	show_track(iTrack);
	term_color_swap();
}

static inline void show_playing()
{
	term_set_cursor(MODE_DESC + 1, 0);
	term_writestring("Now playing: ");
	term_writestring(track_name[playing]);

	term_set_cursor(MODE_DESC, 20);
	term_writestring("Stop: S");

	term_set_cursor(MODE_DESC, 40);
	if (paused)
		term_writestring("Resume: P");
	else
		term_writestring("Pause: P ");
}

void player()
{	
	set_title("Music Player");
	player_display();
	if (playing != TRACKS)
		show_playing();
	
	for (int iTrack = 0; iTrack < TRACKS; iTrack++)
		show_track(iTrack);
	show_track_select(selector);

	while (1)
	{
		hardware_update();
		print_time();
		player_update();

		if (is_msg())
		{
			key_info k = ps2_kbd_driver.code_to_key(ps2_kbd, front_msg());
			pop_msg();

			if (k.release)
				continue;
			switch (k.chr)
			{
			case '\e':
				return;
			case DOWN:
				show_track(selector);
				selector = (selector + 1) % TRACKS;
				show_track_select(selector);
				break;
			case UP:
				show_track(selector);
				selector = (selector + TRACKS - 1) % TRACKS;
				show_track_select(selector);
				break;
			case '\n':
				player_stop();
				playing = selector;
				paused = false;
				show_playing();
				break;
			case 's':
			case 'S':
				if (playing >= TRACKS)
					break;
				player_stop();
				pcspk_driver.stop_sound(pcspk);
				break;
			case 'p':
			case 'P':
				if (playing >= TRACKS)
					break;
				if (paused)
				{
					next_event = 0;
					paused = false;
					term_set_cursor(MODE_DESC, 40);
					term_writestring("Pause: P ");
				}
				else
				{
					pcspk_driver.stop_sound(pcspk);
					paused = true;
					term_set_cursor(MODE_DESC, 40);
					term_writestring("Resume: P");
				}
				break;
			}
		}
	}
}
