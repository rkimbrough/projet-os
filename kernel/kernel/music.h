#ifndef MUSIC_H
#define MUSIC_H 1

#include <stdint.h>

#define EQ_SUBDIV 12
#define RANGE 73

extern uint32_t eq_temperament[RANGE];

typedef enum _note
{
	G2, Ab2, A2, Bb2, B2,
	C3, Db3, D3, Eb3, E3, F3, Gb3, G3, Ab3, A3, Bb3, B3,
	C4, Db4, D4, Eb4, E4, F4, Gb4, G4, Ab4, A4, Bb4, B4,
	C5, Db5, D5, Eb5, E5, F5, Gb5, G5, Ab5, A5, Bb5, B5,
	C6, Db6, D6, Eb6, E6, F6, Gb6, G6, Ab6, A6, Bb6, B6,
	C7, Db7, D7, Eb7, E7, F7, Gb7, G7, Ab7, A7, Bb7, B7,
	C8, Db8, D8, Eb8, E8, F8, Gb8, G8,
	REST
} note;

typedef struct _note_length
{
	unsigned int whole;
	unsigned int fraction;
} note_length;

#endif //MUSIC_H
