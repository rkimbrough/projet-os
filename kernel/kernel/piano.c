#include "modes.h"
#include "hardware.h"
#include "music.h"
#include "ui.h"

#include <kernel/msg.h>

#include <stdbool.h>

static note playing;
static int offset;
static bool synesthesia;

static vga_text_color synesthetic_mapping[EQ_SUBDIV] = {
	VGA_LIGHT_RED,
	VGA_MAGENTA,
	VGA_GREEN,
	VGA_DARK_GREY,
	VGA_BLUE,
	VGA_RED,
	VGA_LIGHT_MAGENTA,
	VGA_LIGHT_BROWN,
	VGA_LIGHT_BLUE,
	VGA_LIGHT_CYAN,
	VGA_BROWN,
	VGA_CYAN
};

static note kcode_to_note(uint8_t row, uint8_t col)
{
	switch (row)
	{
	case 3:
		switch (col)
		{
		case 2:
			return C5;
		case 3:
			return D5;
		case 4:
			return E5;
		case 5:
			return F5;
		case 6:
			return G5;
		case 7:
			return A5;
		case 8:
			return B5;
		case 9:
			return C6;
		case 10:
			return D6;
		case 11:
			return E6;
		case 12:
			return F6;
		case 13:
			return G6;
		default:
			return REST;
		}
	case 5:
		switch (col)
		{
		case 2:
			return G3;
		case 3:
			return A3;
		case 4:
			return B3;
		case 5:
			return C4;
		case 6:
			return D4;
		case 7:
			return E4;
		case 8:
			return F4;
		case 9:
			return G4;
		case 10:
			return A4;
		case 11:
			return B4;
		default:
			return REST;
		}
	case 2:
		switch (col)
		{
		case 3:
			return Db5;
		case 4:
			return Eb5;
		case 6:
			return Gb5;
		case 7:
			return Ab5;
		case 8:
			return Bb5;
		case 10:
			return Db6;
		case 11:
			return Eb6;
		case 13:
			return Gb6;
		default:
			return REST;
		}
	case 4:
		switch (col)
		{
		case 3:
			return Ab3;
		case 4:
			return Bb3;
		case 6:
			return Db4;
		case 7:
			return Eb4;
		case 9:
			return Gb4;
		case 10:
			return Ab4;
		case 11:
			return Bb4;
		default:
			return REST;
		}
	default:
		return REST;
	}
}

static inline void print_quit()
{
	term_set_cursor(MODE_DESC, 0);
	term_writestring("Quit: ESC");
}

static inline void print_syn()
{
	term_set_cursor(MODE_DESC, 40);
	term_writestring(synesthesia ? "Normal Mode: ALT     " : "Synesthesia Mode: ALT");
}

static inline void print_oct_up()
{
	term_set_cursor(MODE_DESC + 1, 0);
	term_writestring("Octave Up: SHIFT");
}

static inline void print_oct_down()
{
	term_set_cursor(MODE_DESC + 1, 40);
	term_writestring("Octave Down: CTRL");
}

static inline void set_playing(note n)
{
	playing = n;
	if (n == REST)
		pcspk_driver.stop_sound(pcspk);
	else
		pcspk_driver.play_sound(pcspk, eq_temperament[playing]);
}

void piano()
{
	offset = 0;
	playing = REST;
	synesthesia = false;
	bool need_resume = player_pause(); //On met le jukebox en pause
	pcspk_driver.stop_sound(pcspk);
	set_title("Piano Mode");

	term_clear_row(MODE_DESC);
	term_clear_row(MODE_DESC + 1);

	print_syn();
	print_quit();
	print_oct_up();
	print_oct_down();

	while (1)
	{
		hardware_update();
		print_time();
		if (is_msg())
		{
			key_info k = ps2_kbd_driver.code_to_key(ps2_kbd, front_msg());
			pop_msg();

			if (k.chr == '\e')
				break;
			if (!k.release && k.kcode_row == 5 && (k.kcode_col == 1 || k.kcode_col == 12))
			{
				if (offset < EQ_SUBDIV)
				{
					offset += EQ_SUBDIV;
					if (playing != REST)
						set_playing(playing + EQ_SUBDIV);

					term_clear_row(MODE_DESC + 1);
					if (offset < EQ_SUBDIV)
						print_oct_up();
					print_oct_down();

				}
				continue;
			}
			if (!k.release && k.kcode_row == 6 && (k.kcode_col == 1 || k.kcode_col == 8))
			{
				if (-offset < EQ_SUBDIV)
				{
					offset -= EQ_SUBDIV;
					if (playing != REST)
						set_playing(playing - EQ_SUBDIV);

					term_clear_row(MODE_DESC + 1);
					print_oct_up();
					if (-offset < EQ_SUBDIV)
						print_oct_down();
				}
				continue;
			}
			if (!k.release && k.kcode_row == 6 && (k.kcode_col == 3 || k.kcode_col == 5))
			{
				synesthesia = !synesthesia;
				print_syn();
				if (playing != REST)
				{
					if (synesthesia)
						print_ascii_pianos_color(synesthetic_mapping[playing % EQ_SUBDIV]);
					else
						print_ascii_pianos();
				}
			}

			note recieved = kcode_to_note(k.kcode_row, k.kcode_col);
			if (recieved != REST)
			{
				recieved += offset;
				if (!k.release && recieved != playing)
				{
					set_playing(recieved);
					if (synesthesia)
						print_ascii_pianos_color(synesthetic_mapping[playing % EQ_SUBDIV]);
				}
				else if (k.release && recieved == playing)
				{
					set_playing(REST);
					if (synesthesia)
						print_ascii_pianos(synesthetic_mapping[playing % EQ_SUBDIV]);
				}
			}
		}
	}

	pcspk_driver.stop_sound(pcspk);
	if (need_resume)
		player_resume();
}
