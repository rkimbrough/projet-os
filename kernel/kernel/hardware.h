#ifndef HARDWARE_H
#define HARDWARE_H 1

#include <kernel/timer.h>
#include <kernel/keyboard.h>
#include <kernel/audio.h>
#include <kernel/tty.h>
#include <kernel/floppy.h>

extern timer_driver pit_driver;
extern timer_driver rtc_driver;
extern kbd_driver ps2_kbd_driver;
extern flpy_driver dma_irq_flpy_driver;
extern audio_driver pcspk_driver;

extern keyboard *ps2_kbd;
extern audio_channel *pcspk;

void hardware_init();
void hardware_update();

#endif //HARDWARE_H
