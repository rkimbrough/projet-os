#ifndef VGA_H
#define VGA_H 1

#include <stdint.h>
#include <stddef.h>

//-- Text mode --
#define VGA_WIDTH	80
#define VGA_HEIGHT	25

//Couleurs et fonctions pour créer des entrées VGA
typedef enum _vga_text_color {
	VGA_BLACK,
	VGA_BLUE,
	VGA_GREEN,
	VGA_CYAN,
	VGA_RED,
	VGA_MAGENTA,
	VGA_BROWN,
	VGA_LIGHT_GREY,
	VGA_DARK_GREY,
	VGA_LIGHT_BLUE,
	VGA_LIGHT_GREEN,
	VGA_LIGHT_CYAN,
	VGA_LIGHT_RED,
	VGA_LIGHT_MAGENTA,
	VGA_LIGHT_BROWN,
	VGA_WHITE
} vga_text_color;
typedef uint8_t display_color;
typedef uint16_t display;

inline display_color vga_entry_color(vga_text_color fg, vga_text_color bg) { return fg | bg << 4; }
inline vga_text_color get_fg_color(display_color color) { return color & 0x0F; }
inline vga_text_color get_bg_color(display_color color) { return color >> 4; }

inline display vga_entry(unsigned char c, display_color color) { return (display)c | (display)color << 8; }
inline unsigned char retrieve_char(display entry) { return entry & 0x00FF; }
inline display_color retrieve_color(display entry) { return entry >> 8; }

//Fonctions à implémenter
void vga_init();

void vga_set_entry_at(char c, display_color color, size_t row, size_t column);
display vga_get_entry_at(size_t row, size_t column);

void vga_show_cursor();
void vga_hide_cursor();
void vga_set_cursor(size_t row, size_t column);

#endif //VGA_H
