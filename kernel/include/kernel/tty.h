#ifndef TTY_H
#define TTY_H 1

#include <kernel/vga.h>

#include <stddef.h>

void term_init();

void term_set_cursor(size_t row, size_t col);
void term_set_color(vga_text_color fg, vga_text_color bg);
display_color term_get_color();
vga_text_color term_get_fg_color();
vga_text_color term_get_bg_color();
void term_color_swap();

void term_clear();
void term_clear_row(size_t row);

void term_putchar(char c);
void term_write(const char *data, size_t size);
void term_writestring(const char *str);

#endif //TTY_H
