#ifndef AUDIO_H
#define AUDIO_H 1

#include <kernel/drivers.h>

#include <stdint.h>

typedef struct _audio_channel audio_channel;
typedef struct _audio_driver
{
	generic_driver base;
	audio_channel *(*add_channel)(const char *name);
	void (*play_sound)(audio_channel *ch, uint32_t freq);
	void (*stop_sound)(audio_channel *ch);
} audio_driver;

#endif //AUDIO_H
