/*
Ce fichier contient des constantes utiles pour la gestion du clavier, ainsi que
des structures permettant une abstraction matérielle du clavier.

Le type incomplet keyboard a vocation a être complété pour chaque type de
clavier pour contenir les informations du matériel. Les échanges sont fait via
un kbd_driver, qui contient des pointeurs vers les fonctions pour interagir
avec cette spécification de keyboard.
On attend du driver de fournir un interrupt qui produit un code en fonction de
la touche pressé, et des fonctions pour :
	- Convertir un code en caractère imprimable
	- Activer un lock (scroll, nums ou caps)
	- Maintenir quelles touches spéciales sont pressées 
*/

#ifndef KEYBOARD_H
#define KEYBOARD_H 1

#include <kernel/drivers.h>
#include <kernel/keys.h>

#include <stdbool.h>
#include <stdint.h>


//-- Constantes --
typedef enum _cursor_movement
{
	LEFT = 1,
	RIGHT,
	UP,
	DOWN,
	PGUP = 21,
	PGDOWN,
	HOME,
	END
} cursor_movement;

typedef enum _lock
{
	SCROLL,
	NUMBER,
	CAPS
} lock;

typedef enum _held
{
	LEFT_SHIFT,
	RIGHT_SHIFT,
	LEFT_CONTROL,
	RIGHT_CONTROL,
	LEFT_ALT,
	RIGHT_ALT
} held;

typedef struct _keyboard keyboard;
typedef struct _kbd_driver
{
	generic_driver base;
	keyboard *(*add_keyboard)(const char *name);
	void (*toggle_lock)(keyboard *k, lock l);
	uint8_t (*check_lock)(keyboard *k, lock l);
	uint8_t (*check_held)(keyboard *k, held h);
	key_info (*code_to_key)(keyboard *k, sc_t sc);
} kbd_driver;

#endif //KEYBOARD_H
