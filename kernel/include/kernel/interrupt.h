#ifndef INTERRUPT_H
#define INTERRUPT_H 1

#include <stdbool.h>

#define IRQ_START 0x20
typedef enum _irq_number
{
	PIT_INT   = 0,
	KBD_INT   = 1,
	FLPY_INT  = 6,
	RTC_INT   = 8,
	MOUSE_INT = 12
} irq_number;


typedef struct _interrupt_frame interrupt_frame;

void send_EOI(unsigned char irq);
//Les deux fonctions suivantes renvoient vrai si l'IRQ doit être ignoré
bool handle_spurious_7();
bool handle_spurious_15();

void set_interrupt(int n_int, void (*isr)(interrupt_frame *));
void enable_irq(irq_number irq);
void disable_irq(irq_number irq);

void enable_nmi();
void disable_nmi();

#endif //INTERRUPT_H
