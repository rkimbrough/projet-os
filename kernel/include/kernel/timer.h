#ifndef TIMER_H
#define TIMER_H 1

#include <kernel/drivers.h>

#include <stdint.h>

typedef struct _timer_driver
{
	generic_driver base;
} timer_driver;

#define SYS_TICK_RATE 1000
extern volatile uint64_t sys_ticks;
#define REAL_TICK_RATE 8196
extern volatile uint64_t real_ticks;

//secondes - minutes - heures - jour de la semaine - jour - mois - année
extern volatile int date[6];

typedef enum _date_field
{
	DATE_SECONDS,
	DATE_MINUTES,
	DATE_HOURS,
	DATE_DAY,
	DATE_MONTH,
	DATE_YEAR
} date_field;

#endif
