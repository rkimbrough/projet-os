#ifndef COMMANDS_H
#define COMMANDS_H 1

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t cmd_data_t;
typedef void (*cmd_sender_t)(cmd_data_t);
typedef struct _cmd_t
{
	cmd_data_t data;
	cmd_sender_t routine;
} cmd_t;

#define QUEUE_SIZE 10
typedef struct _cmdq
{
	cmd_t buff[QUEUE_SIZE];
	volatile uint8_t front;
	volatile uint8_t back;
	volatile uint8_t size;
} cmdq;

void reset_cmd(cmdq *queue);
void queue_cmd(cmdq *queue, cmd_data_t data, cmd_sender_t routine);
cmd_t front_cmd(cmdq *queue);
void pop_cmd(cmdq *queue);
bool has_cmd(cmdq* queue);

#endif //COMMANDS_H
