#ifndef PAGE_ALLOC_H
#define PAGE_ALLOC_H 1

#include <stdint.h>

// numbers of blocks by buddy divided by 32 (4K -> 512K)
#define BUDDY_0_SIZE 0x8000
#define BUDDY_1_SIZE 0x4000 
#define BUDDY_2_SIZE 0x2000 
#define BUDDY_3_SIZE 0x1000 
#define BUDDY_4_SIZE 0x800 
#define BUDDY_5_SIZE 0x400 
#define BUDDY_6_SIZE 0x200 
#define BUDDY_7_SIZE 0x100 

typedef struct _buddy {
	uint32_t buddy_0[BUDDY_0_SIZE]; 
	uint32_t buddy_1[BUDDY_1_SIZE]; 
	uint32_t buddy_2[BUDDY_2_SIZE]; 
	uint32_t buddy_3[BUDDY_3_SIZE]; 
	uint32_t buddy_4[BUDDY_4_SIZE]; 
	uint32_t buddy_5[BUDDY_5_SIZE]; 
	uint32_t buddy_6[BUDDY_6_SIZE]; 
	uint32_t buddy_7[BUDDY_7_SIZE]; 
} buddy;

extern buddy buddy_alloc;

void init_alloc_system(buddy *buddy_alloc);

// helper for modifying the buddies
void access_buddy(int n_buddy, int index, int offset, buddy *buddy_alloc);
// pages are ordered from PD_idx * 1024 + PT_idx
void mark_page_as_used(uint32_t page_number);
void on_page_fault(uint32_t vadd, uint32_t err);

#endif //PAGE_ALLOC_H
