#ifndef KEYS_H
#define KEYS_H 1

#include <stdint.h>
#include <stdbool.h>

//-- Struct pour les scan codes --
typedef struct _sc_t
{
	int size;
	uint8_t code[8];
} sc_t;

inline void copy_sc_t(sc_t *a, sc_t* b)
{
	a->size = b->size;
	for (int i = 0; i < a->size; i++)
		a->code[i] = b->code[i];
}

//-- Struct pour les touches du clavier --
typedef struct _key_info
{
	char chr;
	uint8_t kcode_row : 3;
	uint8_t kcode_col : 5;
	bool release;
} key_info;

#endif //KEYS_H
