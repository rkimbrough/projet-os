#ifndef DRIVERS_H
#define DRIVERS_H 1

typedef struct _generic_driver
{
	char *driver_name;
	void (*driver_init)();
	void (*driver_update)();
	const char *(*get_device_name)(void *device);
} generic_driver;

#endif //DRIVERS_H
