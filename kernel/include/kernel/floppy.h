#pragma once

#include <kernel/drivers.h>

#include <stdbool.h>
#include <stdint.h>

typedef struct _floppy{
    char* name;
} floppy;

typedef struct _flpy_driver{
    generic_driver base;
    uint8_t* (*flpy_read_sector)(uint8_t sectorLBA);
    void (*flpy_write_sector)(uint8_t sectorLBA, uint8_t* data, int size);
} flpy_driver;