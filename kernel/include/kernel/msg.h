#ifndef MSG_H
#define MSG_H 1

#include <kernel/keys.h>

#include <stdbool.h>

void close_msgq();
void open_msgq();
void reset_msgq();
void push_msg(sc_t msg);
sc_t front_msg();
void pop_msg();
bool is_msg();

#endif //MSG_H
