#!/bin/sh
set -e
. ./config.sh
for p in $PROJECT_HEADERS
do
	(cd $p && make install-headers DESTDIR=$SYSROOT)
done
