#!/bin/sh
set -e
. ./config.sh
for p in $SUBPROJECTS
do
	(cd $p && make clean DESTDIR=$SYSROOT)
done

rm -rf sysroot
rm -rf isodir
rm -f ${PROJECT_NAME}.iso
