#!/bin/sh
set -e
. ./headers.sh
for p in $SUBPROJECTS
do
	(cd $p && make install DESTDIR=$SYSROOT)
done

mkdir -p isodir/boot/grub
cp sysroot/boot/${PROJECT_NAME}.kernel isodir/boot/
cp boot/grub/grub.cfg isodir/boot/grub/
grub-mkrescue -o ${PROJECT_NAME}.iso isodir
