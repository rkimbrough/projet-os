# [PianOS](https://git.eleves.ens.fr/rkimbrough/projet-os)

Projet (rock'n'roll) d'Antoine Anastassiades, Yannis Kedadry et Rémy Kimbrough pour le cours de systèmes d'exploitation.

## Compilation du projet
### Dépendances
Sont nécessaire pour compiler ce projet :
- Un shell
- La commande `make`
- La commande `grub-mkrescue`
- Un compilateur C vers `i686-elf`, nommé `i686-elf-gcc` (ainsi que `i686-elf-as` et `i686-elf-ar`), comme obtenu en suivant les indications sur [cette page](https://wiki.osdev.org/GCC_Cross-Compiler)

En outre, nous utilisons QEMU afin de faire tourner notre projet (commande `qemu-system-i386`).

### Compilation
Un fichier `Makefile` se trouve dans le répertoire du projet, permettant les commandes suivantes :
- `make`, `make all` : compile le projet.
- `make run` : compile et exécute le projet à l'aide de QEMU.
- `make lib` : compile la librairie C que nous avons codée et l'installe, ainsi que tous les fichiers d'en-tête, dans un dossier `sysroot/`.
- `make clean` : supprime tous les fichiers créés par les autres commandes.


## Les choix techniques
### GDT / IDT
On a codé le plus de choses possibles en C afin d'avoir des structures et des fonctions qui découpent les données comme il faut, avec uniquement une fonction codée en assembleur pour spécifier au processeur le nouveau GDT/IDT.

Le fichier `interrupt.h` fournit aussi une fonction pour que les drivers puissent installer eux-même l'ISR pour l'interrupt qu'ils utilisent (s'il y en a un) dans l'IDT.

### Les drivers
- Périphériques PS2 : on a réalisé deux drivers, un pour un clavier avec QWERTY pour unique choix de disposition des touches, prenant en compte les majuscules et un pour la souris. Contrairement au clavier qui est spontanément pris en compte par QEMU, la souris a nécessité la mise en place d'une routine d'activation du controlleur PS2. Certaines étapes de cette routine étant assez peu détaillées sur les différentes sources qu'on a pu trouver, on a fait le choix de supprimer des étapes qui n'étaient pas indispensables.

- PIT : On utilise les canaux 0 (interrupt, programmé pour envoyer des tics) et 2 (PC speaker, programmé pour produire un signal carré). On permet de programmer la fréquence de chaque canal, et l'ISR se contente de compter les tics envoyés sur le canal 0 (initialisé avec une fréquence à 1kHz). Cela pourrait-être utilisé à l'avenir pour avoir, par exemple, un scheduler.

- RTC : On le programme pour lancer des interrupts à chaque tic (avec une fréquence de 8192Hz) ainsi qu'à chaque seconde, lorsque la date est mise à jour dans la mémoire CMOS. L'ISR se charge dans le premier cas d'augmenter un compteur, dans le deuxième d'actualiser la date en lisant dans la CMOS.

- Le Floppy Disk Controller (FDC): Le FDC ayant été réalisé en parallèle du paging, on a décidé de le faire sans avoir encore de DMA. On se contente donc juste d'avoir un grand buffer et une tête de lecture en guise de DMA. À l'avenir, on pourra rajouter un DMA et il suffira ensuite de modifier les fonctions suivantes dans les fichiers `floppy.c` et `floppy.h` du driver.
```c
// Initialise le DMA pour le floppy
void flpy_init_dma();

// Prépare le DMA pour la lecture du floppy
void flpy_dma_read();

// Prépare le DMA pour l'écriture du floppy
void flpy_dma_write();
```
Notre FDC n'est donc pas entièrement opérationnel mais la structure de base est implémentée.

- PC Speaker : On le programme pour que le signal d'entrée soit lu sur le canal 2 du PIT. On peut alors changer la note en changeant la fréquence du signal sur ce canal, et spécifier lorsque l'on veut (ou ne veut pas) jouer le son.

### Paging
La gestion de la mémoire via un système de paging est une étape que nous souhaitions réaliser pour notre système d'exploitation mais que nous n'avons pas réussi à finaliser.

Étant restés en 32-bit, nous organisons notre paging en deux tables, le Paging Directory dont l'adresse est stockée au sein du registre `cr3`, et les Paging Table, vers lesquelles pointent les entrées du Paging Directory.  
Cela nous aurait permis d'allouer des pages de 4KB.  

Dans le dossier `kernel/arch/i386/paging`, les fichiers `init.c` ainsi que `paging_setup.s` servent respectivement à initialiser les tables (en mettant l'espace noyau dans la partie haute de la mémoire) et à activer le paging en mettant à jour les différents registres du processeur.  
Le fichier `page_allocation.c` est le début d'une ébauche d'algorithme d'allocation de mémoire virtuelle que nous comptions utiliser afin de gérer les page faults.  
Nous avons fait le choix de coder un système de buddy allocator similaire à celui trouvé dans le noyau Linux.
Ce système nous aurait permis d'allouer rapidement les plages de mémoire demandées par des processus, mais nous nous sommes heurtés à la difficulté de pouvoir à la fois garder la trace des pages à allouer et à celle de décider sur quelle plage de mémoire les pages allouées se situeront.

Nous n'avons malheureusement pas réussi à finir ce système de paging à temps, mais c'est un point sur lequel nous pourrons travailler si nous souhaitons améliorer PianOS.

## La spécificité de PianOS
N'ayant pas programmé assez de fonctionnalités pour passer en user mode, mais ayant tout de même un certain nombre de drivers, nous avons fait le choix d'exploiter ceux-ci pour présenter une interface un peu originale. Au démarrage, on peut choisir entre trois modes possibles :
- Le Piano Mode, dans lequel nous avons relié les appuis sur les touches du clavier à des notes jouées sur le PC Speaker. La disposition des notes est comme sur un vrai piano : la rangée AZERTYUIOP (ou QWERTYUIOP selon la disposition des touches) fait les touches blanches de Do5 à Sol6 et la rangée des chiffres juste au dessus fait les touches noires.
De même, la rangée WXCVBN (ou ZXCVBNM selon la disposition) fait les touches blanches de Sol3 à Si4, et la rangée juste au dessus fait les touches noires correspondantes.
On peut aussi changer d'octave avec SHIFT/CTRL.

- Le Text Mode, dans lequel on peut écrire et effacer ce qu'on veut. Ce n'est pas le plus complet, mais c'est le vestige de ce qu'on avait avant le piano. On affiche aussi le curseur.

- Le Music Player, qui peut jouer une musique parmi un choix foisonnant de 3 (trois !) titres. On peut arrêter, mettre en pause et la musique continue même à jouer lorsqu'on quitte ce mode (mais est mise en pause lorsqu'on utilise le Piano Mode). C'est peut-être le morceau de code "haut niveau" le plus intéressant des trois car il faut constamment mettre à jour ce qui est en train d'être joué.
