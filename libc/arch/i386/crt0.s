.section	.text
.global		_start
type		_start, @function
_start:
	# Préparation de la pile d'appel
	mov		0, %ebp
	push	%ebp
	push	%ebp
	mov		%esp, %ebp

	# Sauvegarde des arguments de main
	push	%esi
	push	%edi

	# Initialisation puis lancement du programme
	# call	init_stdlib
	call	_init

	pop		%edi
	pop		%esi
	call	main

	mov		%eax, %edi
	call	exit

.size		_start, . - _start

