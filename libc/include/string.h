#ifndef STRING_H
#define STRING_H 1

#include <stddef.h>
#include <stdbool.h>

// get length of string
size_t strlen(const char *str);

// compare strings
int strcmp(const char* s, const char* t);

#endif //STRING_H
