#ifndef _STDIO_H
#define _STDIO_H 1
 
#define EOF (-1)
 
// Formated print
int printf(const char* __restrict, ...);

// Print a char
int putchar(int c);

// Print a string
int puts(const char* string);
 
#endif