#include <string.h>

int strcmp(const char *s, const char *t)
{
	while (*s && *s == *t)
	{
		s++;
		t++;
	}
	
	return ((unsigned char)*t) - ((unsigned char)*s);
}
