all:
	./build.sh
run:
	./run.sh
lib:
	./lib.sh
clean:
	./clean.sh

.PHONY: all lib run clean
